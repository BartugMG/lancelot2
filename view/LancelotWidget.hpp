#pragma once

#include <Wt/WContainerWidget.h>
#include "Session.hpp"

namespace Lancelot
{
class CharacterCardWidget;
class CharacterCreatorWidget;
using namespace Wt::Core;

class LancelotWidget : public Wt::WContainerWidget
{
public:
    LancelotWidget();
    void handleInternalPathChange(const std::string& path);

private:
    bool characterExist() const;

    observing_ptr<Wt::WStackedWidget> m_mainStack;
    observing_ptr<CharacterCardWidget> m_characterCard;
    observing_ptr<CharacterCreatorWidget> m_characterCreator;

    observing_ptr<Wt::WMenuItem> m_characterCardMenuItem;
};


}
