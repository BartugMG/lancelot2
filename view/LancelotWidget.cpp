#include "LancelotWidget.hpp"
#include <Wt/WNavigationBar.h>
#include <Wt/Auth/AuthWidget.h>
#include <Wt/WLineEdit.h>
#include <Wt/WText.h>
#include "CharacterCardView/CharacterCardWidget.hpp"
#include "CharacterCardView/CharacterCreatorWidget.hpp"
#include "LancelotApp.hpp"
#include <Wt/WStackedWidget.h>
#include <Wt/WMenu.h>
#include "CommonModelsFillers.hpp"

namespace Lancelot
{
using namespace Wt;
using namespace Wt::Auth;

LancelotWidget::LancelotWidget():
    m_characterCard(nullptr),
    m_characterCreator(nullptr)
{
    // navigation bar
    auto navigation = addWidget(std::make_unique<WNavigationBar>());
    navigation->setTitle("Lancelot",
                         WLink(LinkType::InternalPath, "/"));

    navigation->setResponsive(true);

    // left menu
    auto leftMenu = navigation->addMenu(std::make_unique<WMenu>());

    m_characterCardMenuItem = leftMenu->addItem("Karta postaci");
    m_characterCardMenuItem->setLink(WLink(LinkType::InternalPath, "/card"));

    // right menu
    auto authWidget = std::make_unique<Auth::AuthWidget>(Session::authService(),
                                                         LancelotApp::session().users(),
                                                         LancelotApp::session().login());
    navigation->addFormField(std::move(authWidget), AlignmentFlag::Right);

    // rest of widgets
    m_mainStack = addWidget(std::make_unique<WStackedWidget>());
    m_mainStack->addWidget(std::make_unique<WText>("<h1>Witaj na stronie głównej!</h1>"));

}

void LancelotWidget::handleInternalPathChange(const std::string &path)
{
    if (path == "/card")
    {
        m_characterCardMenuItem->setStyleClass("active");

        if (characterExist())
        {
            AgregateModelsFiller::instance();
            if (not m_characterCard)
            {
                m_characterCard = m_mainStack->addWidget(
                        std::make_unique<CharacterCardWidget>());
            }
            m_mainStack->setCurrentWidget(m_characterCard.get());
            m_characterCard->update();
        }

        else
        {
            LancelotApp::instance()->setInternalPath("/create");
            if (not m_characterCreator)
            {
                m_characterCreator = m_mainStack->addWidget(
                        std::make_unique<CharacterCreatorWidget>());
            }
            m_mainStack->setCurrentWidget(m_characterCreator.get());
            m_characterCreator->update();
        }
    }
    else
    {
        m_characterCardMenuItem->setStyleClass("");
        m_mainStack->setCurrentIndex(0);
    }

}

bool LancelotWidget::characterExist() const
{
    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);
    return not session.user()->m_characters.empty();
}

}
