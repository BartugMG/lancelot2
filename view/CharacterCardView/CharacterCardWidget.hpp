#pragma once
#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>
#include <Wt/WGridLayout.h>
#include <Wt/WTemplate.h>
#include "Session.hpp"
#include "Parameter.hpp"
#include "Utility.hpp"

namespace Lancelot
{
using namespace Wt::Core;
class GeneralInfoWidget;
class ParametersWidget;
class StatusPanelWidget;
class EquipmentWidget;
class PouchWidget;

class CharacterCardWidget : public Wt::WContainerWidget
{
public:
    explicit CharacterCardWidget();
    void update();

private:
    observing_ptr<Wt::WVBoxLayout> m_mainLayout;

    observing_ptr<GeneralInfoWidget> m_generalInfo;
    observing_ptr<ParametersWidget> m_parameters;
    observing_ptr<StatusPanelWidget> m_changes;
    observing_ptr<EquipmentWidget> m_eq;
    observing_ptr<PouchWidget> m_pouch;
    Wt::Dbo::ptr<Character> m_character;

    void initialCharacterConfig();
    void showNewItemDialog();
    void showPouchChangeDialog();
    void showNewStatChangeDialog();
    void showTurnEndDialog();
    void showAddExpDialog();
    void updateAfterEqOrNewStatChange();

    void addFeaturesTab(Wt::WTabWidget* tabWidget);
    void addEquipmentTab(Wt::WTabWidget* tabWidget);
};

}
