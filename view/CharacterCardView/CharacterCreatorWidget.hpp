#pragma once

#include <Wt/WContainerWidget.h>

namespace Lancelot
{
using namespace Wt::Core;
class GeneralInfoWidget;
class ParametersWidget;

class CharacterCreatorWidget : public Wt::WContainerWidget
{
public:
    CharacterCreatorWidget();
    void update();

private:
    void createCharacter();
    bool validateChoices(const std::string& name);

    observing_ptr<GeneralInfoWidget> m_generalInfo;
    observing_ptr<ParametersWidget> m_parameters;

};

}
