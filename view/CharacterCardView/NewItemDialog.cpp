#include "NewItemDialog.hpp"
#include "LancelotApp.hpp"
#include "Item.hpp"
#include "Potion.hpp"
#include "Jewellery.hpp"
#include "ParameterModel.hpp"
#include "Armor.hpp"
#include "Weapon.hpp"
#include <algorithm>
#include <Wt/WPushButton.h>
#include <Wt/WComboBox.h>
#include <Wt/WStackedWidget.h>
#include <Wt/WTemplate.h>
#include <Wt/WLineEdit.h>
#include <Wt/WLabel.h>
#include <Wt/WTextArea.h>
#include <Wt/WSpinBox.h>
#include <Wt/WDoubleSpinBox.h>
#include <Wt/WLabel.h>
#include <Wt/WRegExpValidator.h>
#include <Wt/WHBoxLayout.h>
#include <Wt/WCheckBox.h>
#include <boost/optional.hpp>

namespace Lancelot
{
using namespace Wt;

namespace {

}

NewItemDialog::NewItemDialog(Dbo::ptr<Equipment> eq):
    WDialog("Nowy przedmiot"),
    m_eq(std::move(eq))
{
    rejectWhenEscapePressed();
    addStyleClass("modal-diff");
    contents()->addStyleClass("modal-body-diff");

    createComboBox();
    createStack();

    auto cancel = footer()->addWidget(std::make_unique<WPushButton>("Anuluj"));
    cancel->clicked().connect(this, &NewItemDialog::reject);

    auto ok = footer()->addWidget(std::make_unique<WPushButton>("OK"));
    ok->clicked().connect(this, &NewItemDialog::createFromView);
}

void NewItemDialog::createComboBox()
{
    auto combobox = contents()->addWidget(std::make_unique<WComboBox>());
    combobox->setMargin(10, Side::Bottom);
    combobox->addItem("Przedmiot");
    combobox->addItem("Mikstura");
    combobox->addItem("Biżuteria");
    combobox->addItem("Zbroja");
    combobox->addItem("Broń");

    combobox->activated().connect([=](int selected) {
        m_stack->setCurrentIndex(selected);
    });
}

void NewItemDialog::createStack()
{
    m_stack = contents()->addWidget(std::make_unique<WStackedWidget>());
    m_forms[0] = m_stack->addWidget(std::make_unique<ItemForm>());
    m_forms[1] = m_stack->addWidget(std::make_unique<PotionForm>());
    m_forms[2] = m_stack->addWidget(std::make_unique<JewelleryForm>());
    m_forms[3] = m_stack->addWidget(std::make_unique<ArmorForm>());
    m_forms[4] = m_stack->addWidget(std::make_unique<WeaponForm>());
}

void NewItemDialog::createFromView()
{
    auto currentForm = m_stack->currentIndex();
    if (not m_forms[currentForm]->create(m_eq))
    {
        return;
    }

    itemCreated.emit();
    accept();
}

/////////////////////////////////////////////////////////////////////////////////////

ItemForm::ItemForm()
{
    m_name = Utility::addFormToContainerWidget<WLineEdit>("Nazwa:", this);
    m_name->setMaxLength(30);
    m_name->setValidator(std::make_unique<WRegExpValidator>(Utility::NAME_REGEX));

    m_desctiption = Utility::addFormToContainerWidget<WTextArea>("Opis:", this);
    m_desctiption->resize(m_desctiption->width(), 40);

    m_amount = Utility::addFormToContainerWidget<WSpinBox>("Liczba:", this);
    m_amount->setMaximum(999);
    m_amount->setMinimum(1);
    m_amount->setValue(1);

    m_singularWeight  = Utility::addFormToContainerWidget<WDoubleSpinBox>("Waga pojedynczego:", this);
    m_singularWeight->setMinimum(0);
}

bool ItemForm::create(const Wt::Dbo::ptr<Equipment>& eq) const
{
    if (not validate())
    {
        return false;
    }

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);

    auto item = session.add(std::make_unique<Item>(
            m_name->valueText().toUTF8(),
            m_desctiption->valueText().toUTF8(),
            m_amount->value(),
            m_singularWeight->value()));

    eq.modify()->items.insert(item);

    return true;
}

bool ItemForm::validate() const
{
    if (m_name->validate() != ValidationState::Valid or m_name->text().empty())
    {
        WMessageBox::show("Błąd",
                          "Nieprawidłowa wartośc w polu Nazwa.",
                          Wt::StandardButton::Ok);
        return false;
    }

    return validateForm(m_amount, "Liczba") and
           validateForm(m_singularWeight, "Waga pojedynczego");
}

bool ItemForm::validateForm(WAbstractSpinBox* form, const WString& fieldName) const
{
    if (form->validate() != ValidationState::Valid)
    {
        WMessageBox::show("Błąd",
                          WString("Nieprawidłowa wartośc w polu {1}.").arg(fieldName),
                          Wt::StandardButton::Ok);

        return false;
    }
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

PotionForm::PotionForm()
{
    m_status = addWidget(std::make_unique<StatusChangeForm>(true, NameEditor::NotExisting));
}

bool PotionForm::create(const Wt::Dbo::ptr<Equipment>& eq) const
{
    if (not validate())
    {
        return false;
    }

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);

    auto potion = session.add(std::make_unique<Potion>(
            m_name->valueText().toUTF8(),
            m_desctiption->valueText().toUTF8(),
            m_amount->value(),
            m_singularWeight->value()));

    auto statusChange = m_status->create(WString("[{1}]").arg(m_name->valueText()));
    potion.modify()->statusChange = statusChange;

    eq.modify()->potions.insert(potion);

    return true;
}

bool PotionForm::validate() const
{
    if (not m_status->validate())
    {
        StatusChangeForm::showValidationError();
        return false;
    }

    return ItemForm::validate();
}

/////////////////////////////////////////////////////////////////////////////////////

JewelleryForm::JewelleryForm()
{
    m_type = Utility::addFormToContainerWidget<WComboBox>("Typ biżuterii:", this);
    m_type->addItem(toString(JewelleryType::Amulet));
    m_type->addItem(toString(JewelleryType::Ring));

    m_status = addWidget(std::make_unique<StatusChangeForm>(false, NameEditor::Checkbox));
}

bool JewelleryForm::create(const Wt::Dbo::ptr<Equipment>& eq) const
{
    if (not validate())
    {
        return false;
    }

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);

    auto jewellery = session.add(std::make_unique<Jewellery>(
            m_name->valueText().toUTF8(),
            m_desctiption->valueText().toUTF8(),
            m_amount->value(),
            m_singularWeight->value(),
            static_cast<JewelleryType>(m_type->currentIndex())));

    auto statusChange = m_status->create(WString("[{1}]").arg(m_name->valueText()));
    jewellery.modify()->statusChange = statusChange;

    eq.modify()->jewellery.insert(jewellery);

    return true;
}

bool JewelleryForm::validate() const
{
    if (not m_status->validate())
    {
        StatusChangeForm::showValidationError();
        return false;
    }

    return ItemForm::validate();
}

/////////////////////////////////////////////////////////////////////////////////////

ArmorForm::ArmorForm()
{
    m_type = Utility::addFormToContainerWidget<WComboBox>("Typ pancerza:", this);
    for (auto type : { ArmorType::Full, ArmorType::Helmet, ArmorType::Cuirass,
                       ArmorType::Gloves, ArmorType::Legs, ArmorType::Boots,
                       ArmorType::Shield })
    {
        m_type->addItem(toString(type));
    }

    m_aclass = Utility::addFormToContainerWidget<WComboBox>("Klasa pancerza:", this);
    for (auto aclass : { ArmorClass::None, ArmorClass::Light,
                         ArmorClass::Medium, ArmorClass::Heavy })
    {
        m_aclass->addItem(toString(aclass));
    }

    m_reduction = Utility::addFormToContainerWidget<WSpinBox>("Redukcja obrażeń:", this);
    m_reduction->setMaximum(0);
    m_reduction->setMinimum(-50);

    m_status = addWidget(std::make_unique<StatusChangeForm>(false, NameEditor::Checkbox));
}

bool ArmorForm::create(const Wt::Dbo::ptr<Equipment>& eq) const
{
    if (not validate())
    {
        return false;
    }

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);

    auto armor = session.add(std::make_unique<Armor>(
            m_name->valueText().toUTF8(),
            m_desctiption->valueText().toUTF8(),
            m_amount->value(),
            m_singularWeight->value(),
            static_cast<ArmorType>(m_type->currentIndex()),
            static_cast<ArmorClass>(m_aclass->currentIndex()),
            m_reduction->value()));

    auto statusChange = m_status->create(WString("[{1}]").arg(m_name->valueText()));
    armor.modify()->statusChange = statusChange;

    eq.modify()->armor.insert(armor);

    return true;
}

bool ArmorForm::validate() const
{
    if (not m_status->validate())
    {
        StatusChangeForm::showValidationError();
        return false;
    }

    return ItemForm::validate() and validateForm(m_reduction, "Redukcja obrażeń");
}

/////////////////////////////////////////////////////////////////////////////////////

WeaponForm::WeaponForm()
{
    m_type = Utility::addFormToContainerWidget<WComboBox>("Typ broni:", this);

    for (auto type : { WeaponType::Dagger, WeaponType::ShortSword, WeaponType::Sword,
                       WeaponType::Axe, WeaponType::Hammer, WeaponType::TwoHSword,
                       WeaponType::TwoHAxe, WeaponType::TwoHHammer, WeaponType::Thrown,
                       WeaponType::Lance, WeaponType::Kusarigama, WeaponType::Bow,
                       WeaponType::Crossbow, WeaponType::Arrow, WeaponType::Bolt,
                       WeaponType::Gun, WeaponType::Grenade, WeaponType::Other,
                       WeaponType::Ultimate })
    {
        m_type->addItem(toString(type));
    }

    prepareDiceForm();

    m_modifier = Utility::addFormToContainerWidget<WSpinBox>("Modyfikator obrażeń:", this);
    m_modifier->setMinimum(-20);
    m_modifier->setMaximum(20);

    prepareDurabilityForm();
}

void WeaponForm::prepareDiceForm()
{
    auto diceContainer = addWidget(std::make_unique<WContainerWidget>());
    auto diceLayout = diceContainer->setLayout(std::make_unique<WHBoxLayout>());
    m_dices = Utility::addFormToContainerWidget<WSpinBox>("Liczba kości:", diceLayout);
    m_dices->setMinimum(1);
    m_dices->setMaximum(20);
    m_dices->setValue(1);
    m_sides = Utility::addFormToContainerWidget<WSpinBox>("Liczba oczek:", diceLayout);
    m_sides->setMinimum(1);
    m_sides->setMaximum(100);
    m_sides->setValue(6);
}

void WeaponForm::prepareDurabilityForm()
{
    auto durabilityBox = addWidget(std::make_unique<WCheckBox>("<b>Skończona wytrzymałość</b>"));
    durabilityBox->setTextFormat(TextFormat::XHTML);
    durabilityBox->setMargin(5, Side::Top);
    durabilityBox->setMargin(7, Side::Bottom);
    durabilityBox->checked().connect([this](){ m_durability->show(); });
    durabilityBox->unChecked().connect([this](){ m_durability->hide(); });
    addWidget(std::make_unique<WBreak>());

    m_durability = Utility::addFormToContainerWidget<WSpinBox>("Wytrzymałość:", this);
    m_durability->setMinimum(1);
    m_durability->setMaximum(100);
    m_durability->setValue(20);
    m_durability->hide();
}


bool WeaponForm::create(const Wt::Dbo::ptr<Equipment>& eq) const
{
    if (not validate())
    {
        return false;
    }

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);

    using DurabilityOpt = boost::optional<int>;
    DurabilityOpt durability = m_durability->isHidden() ? DurabilityOpt(boost::none) :
                                                        m_durability->value();

    auto weapon = session.add<Weapon>(std::make_unique<Weapon>(
            m_name->valueText().toUTF8(),
            m_desctiption->valueText().toUTF8(),
            m_amount->value(),
            m_singularWeight->value(),
            static_cast<WeaponType>(m_type->currentIndex()),
            Dice{m_dices->value(), m_sides->value()},
            m_modifier->value(),
            durability));

    eq.modify()->weapons.insert(weapon);

    return true;
}

bool WeaponForm::validate() const
{
    return ItemForm::validate() and
        validateForm(m_dices, "Liczba kości") and
        validateForm(m_sides, "Liczba oczek") and
        validateForm(m_modifier, "Modyfikator obrażeń") and
        (m_durability->isHidden() or validateForm(m_durability, "Wytrzymałość"));
}

}

