#include "WeaponWidget.hpp"

namespace Lancelot
{
using namespace std::string_literals;
using namespace Wt;

WeaponWidget::WeaponWidget(Wt::Dbo::ptr<Weapon> weapon):
    GenericItemWidget(std::move(weapon))
{
    addWeaponDamageItem();
    addDurabilityItem();
    addWeaponDescription();
    addWeaponWearingItem();
}

void WeaponWidget::addWeaponDamageItem()
{
    auto diceStr = WString("Obrażenia: {1}").arg(m_item->dice().asString());

    if (auto mod = m_item->modifier(); mod != 0)
    {
        auto sign = WString("+");

        if(mod < 0)
        {
            sign = WString("-");
            mod = -mod;
        }

        diceStr += WString(" {1} {2}").arg(sign).arg(mod);
    }

    auto diceItem = m_menu->insertItem(0, diceStr);
    diceItem->setDisabled(true);
}

void WeaponWidget::addDurabilityItem()
{
    auto durabilityStr = m_item->durability().is_initialized() ?
                         std::to_string(m_item->durability().get()) :
                         "-"s;

    auto durabilityItem = m_menu->insertItem(1, WString("Wytrzymałość: {1}").arg(durabilityStr));
    durabilityItem->setDisabled(true);
}

void WeaponWidget::addWeaponDescription()
{
    m_menu->insertItem(3, "Opis")->clicked().connect([&](){
        Wt::WMessageBox::show(
            m_item->name(),
            WString("<p>Typ broni: {1}</p>"
                    "<p></p>"
                    "<p>{2}</p>"
            ).arg(toString(m_item->type())).arg(m_item->description()),
            Wt::StandardButton::Ok);
    });
}

void WeaponWidget::addWeaponWearingItem()
{
    auto label = m_item->isEquipped() ? "Zdejmij" : "Załóż";

    m_menu->insertItem(4, label)->clicked().connect([&](){
        m_equiped.emit(m_item);
    });
}

}
