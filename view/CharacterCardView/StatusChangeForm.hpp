#pragma once
#include <Wt/WContainerWidget.h>
#include "StatusChange.hpp"
#include "Parameter.hpp"

namespace Lancelot
{
using namespace Wt::Core;

class ParameterChangeForm : public Wt::WContainerWidget
{
public:
    ParameterChangeForm();
    ParameterValue getChosenValue() const;
    bool validate() const;

private:
    observing_ptr<Wt::WComboBox> m_params;
    observing_ptr<Wt::WSpinBox> m_value;
};

/////////////////////////////////////////////////////////////////////////////////////

enum class NameEditor
{
    NotExisting,
    Existing,
    Checkbox
};

class StatusChangeForm: public Wt::WContainerWidget
{
public:
    explicit StatusChangeForm(bool isFinite, NameEditor nameEditor);
    Wt::Dbo::ptr<StatusChange> create(const Wt::WString& name = "") const;
    bool validate() const;

    static void showValidationError();

protected:
    void addParam();
    void rmParam();

    observing_ptr<Wt::WLineEdit> m_name;
    observing_ptr<Wt::WTextArea> m_description;
    observing_ptr<Wt::WSpinBox> m_duration;
    observing_ptr<WContainerWidget> m_paramContainer;
    std::vector<observing_ptr<ParameterChangeForm>> m_parameters;
};

}

