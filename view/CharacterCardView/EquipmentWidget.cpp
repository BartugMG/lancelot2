#include "EquipmentWidget.hpp"
#include <Wt/WVBoxLayout.h>
#include <Wt/WPanel.h>
#include "LancelotApp.hpp"
#include "WeaponWidget.hpp"
#include "ArmorWidget.hpp"
#include "JewelleryWidget.hpp"
#include "PotionWidget.hpp"
#include "Item.hpp"
#include "Utility.hpp"

namespace Lancelot
{
using namespace Wt;

EquipmentWidget::EquipmentWidget(Wt::Dbo::ptr<Equipment> eq):
    m_eq(std::move(eq)),
    m_equippedWeapons{nullptr, nullptr, nullptr},
    m_equippedJewellery{nullptr, nullptr, nullptr}
{
    m_layout = setLayout(std::make_unique<WVBoxLayout>());
    m_layout->setContentsMargins(0, 0, 0, 0);

    m_weaponContainer = createPanelWithContainer("Broń"s);
    m_wearableContainer = createPanelWithContainer("Ekwipunek"s);
    m_potionContainer = createPanelWithContainer("Mikstury"s);
    m_itemContainer = createPanelWithContainer("Pozostałe przedmioty"s);

    m_equippedWeapons.findEquippedWeapons(m_eq->weapons);
    m_equippedArmor.findEquippedArmor(m_eq->armor);
    m_equippedJewellery.findEquippedJewellery(m_eq->jewellery);
}

observing_ptr<WContainerWidget> EquipmentWidget::createPanelWithContainer(const std::string& title)
{
    auto panel = m_layout->addWidget(std::make_unique<WPanel>());
    panel->setTitle(title);
    panel->setCollapsible(true);

    return panel->setCentralWidget(std::make_unique<WContainerWidget>());
}

void EquipmentWidget::update()
{
    m_itemContainer->clear();
    Dbo::Transaction t(LancelotApp::session());
    updateWeapons();
    updateWearable();
    updatePotions();
    updateItems();

    equipmentChanged.emit();
}

void EquipmentWidget::updateWeapons()
{
    m_weaponContainer->clear();

    for(auto it = m_eq->weapons.begin(); it != m_eq->weapons.end(); ++it)
    {
        auto container = (*it)->isEquipped() ? m_weaponContainer : m_itemContainer;
        auto ww = container->addWidget(std::make_unique<WeaponWidget>(*it));
        ww->equiped().connect(this, &EquipmentWidget::handleWeaponChange);
    }
}

void EquipmentWidget::updateWearable()
{
    m_wearableContainer->clear();
    for(auto it = m_eq->armor.begin(); it != m_eq->armor.end(); ++it)
    {
        auto container = (*it)->isEquipped() ? m_wearableContainer : m_itemContainer;
        auto aw = container->addWidget(std::make_unique<ArmorWidget>(*it));
        aw->equiped().connect(this, &EquipmentWidget::handleArmorChange);
    }
    for(auto it = m_eq->jewellery.begin(); it != m_eq->jewellery.end(); ++it)
    {
        auto container = (*it)->isEquipped() ? m_wearableContainer : m_itemContainer;
        auto jw = container->addWidget(std::make_unique<JewelleryWidget>(*it));
        jw->equiped().connect(this, &EquipmentWidget::handleJewelleryChange);
    }

}

void EquipmentWidget::updatePotions()
{
    m_potionContainer->clear();
    for(auto it = m_eq->potions.begin(); it != m_eq->potions.end(); ++it)
    {
        auto pw = m_potionContainer->addWidget(std::make_unique<PotionWidget>(*it));
        pw->used.connect([&](Dbo::ptr<StatusChange> sc) {
            updatePotions();
            potionUsed.emit(sc);
        });
    }
}

void EquipmentWidget::updateItems()
{
    using ItemWidget = GenericItemWidget<Item>;
    for(auto it = m_eq->items.begin(); it != m_eq->items.end(); ++it)
    {
        m_itemContainer->addWidget(std::make_unique<ItemWidget>(*it));
    }
}

void EquipmentWidget::handleWeaponChange(Wt::Dbo::ptr<Weapon> weapon)
{
    weapon.modify()->flipEquippedState();
    m_equippedWeapons.weaponChange(weapon);
    update();
}

void EquipmentWidget::handleArmorChange(Wt::Dbo::ptr<Armor> armor)
{
    armor.modify()->flipEquippedState();
    m_equippedArmor.armorChange(armor);
    update();
}

void EquipmentWidget::handleJewelleryChange(Wt::Dbo::ptr<Jewellery> jewellery)
{
    jewellery.modify()->flipEquippedState();
    m_equippedJewellery.jewelleryChange(jewellery);
    update();
}

///////////////////////////////////////////////////////////////////////////////////////////

void EquippedWeapons::findEquippedWeapons(const Dbo::collection <Dbo::ptr<Weapon>>& weapons)
{
    Dbo::Transaction t(LancelotApp::session());

    for(const auto& weapon : weapons)
    {
        if (weapon->isEquipped())
        {
            if (isMainHand(weapon->type()))
            {
                mainHand = weapon;
            }

            if (isOffHand(weapon->type()))
            {
                offHand = weapon;
            }

            if (isInQuiver(weapon->type()))
            {
                inQuiver = weapon;
            }
        }
    }
}

void EquippedWeapons::weaponChange(const Wt::Dbo::ptr<Weapon>& weapon)
{
    if (isMainHand(weapon->type()))
    {
        weaponChangeImpl(mainHand, weapon);
    }

    if (isOffHand(weapon->type()))
    {
        weaponChangeImpl(offHand, weapon);
    }

    if (isInQuiver(weapon->type()))
    {
        weaponChangeImpl(inQuiver, weapon);
    }
}

void EquippedWeapons::weaponChangeImpl(Wt::Dbo::ptr<Weapon>& equippedWeapon,
                                       const Wt::Dbo::ptr<Weapon>& weaponToEquip)
{
    if (not weaponToEquip->isEquipped())
    {
        if (weaponToEquip == equippedWeapon)
        {
            equippedWeapon = nullptr;
            return;
        }
        else
        {
            throw std::runtime_error("This shouldn't happend");
        }
    }

    if (equippedWeapon)
    {
        equippedWeapon.modify()->flipEquippedState();
    }
    equippedWeapon = weaponToEquip;
}

bool EquippedWeapons::isMainHand(WeaponType type)
{
    switch (type)
    {
        case WeaponType::Sword:
        case WeaponType::Axe:
        case WeaponType::Hammer:
        case WeaponType::TwoHSword:
        case WeaponType::TwoHAxe:
        case WeaponType::TwoHHammer:
        case WeaponType::Lance:
        case WeaponType::Kusarigama:
        case WeaponType::Bow:
        case WeaponType::Crossbow:
        case WeaponType::Gun:
        case WeaponType::Other:
        case WeaponType::Ultimate:
            return true;
        default:
            return false;
    }
}

bool EquippedWeapons::isOffHand(WeaponType type)
{
    switch (type)
    {
        case WeaponType::Dagger:
        case WeaponType::ShortSword:
        case WeaponType::Thrown:
        case WeaponType::Kusarigama:
        case WeaponType::Grenade:
            return true;
        default:
            return false;
    }
}

bool EquippedWeapons::isInQuiver(WeaponType type)
{
    return type == WeaponType::Arrow or
           type == WeaponType::Bolt;
}

///////////////////////////////////////////////////////////////////////////////////////////

void EquippedArmor::findEquippedArmor(const Wt::Dbo::collection<Wt::Dbo::ptr<Armor>>& armor)
{
    Utility::valueEmplacer(onCharacter, nullptr, ArmorType::Full, ArmorType::Helmet, ArmorType::Cuirass,
            ArmorType::Gloves, ArmorType::Legs, ArmorType::Boots, ArmorType::Shield);

    Dbo::Transaction t(LancelotApp::session());

    for(const auto& armorPiece : armor)
    {
        if (armorPiece->isEquipped())
        {
            onCharacter[armorPiece->type()] = armorPiece;
        }
    }
}

void EquippedArmor::armorChange(const Wt::Dbo::ptr<Armor>& armorPiece)
{
    auto& current = onCharacter.at(armorPiece->type());

    if (not armorPiece->isEquipped())
    {
        if (armorPiece == current)
        {
           current = nullptr;
           return;
        }
        else
        {
            throw std::runtime_error("This shouldn't happend");
        }
    }

    if (current)
    {
        current.modify()->flipEquippedState();
    }

    current = armorPiece;
}

///////////////////////////////////////////////////////////////////////////////////////////

void EquippedJewellery::findEquippedJewellery(const Dbo::collection<Dbo::ptr<Jewellery>>& jewellery)
{
    Dbo::Transaction t(LancelotApp::session());

    for (const auto& jewelleryPiece : jewellery)
    {
        if (jewelleryPiece->isEquipped())
        {
            if (jewelleryPiece->type() == JewelleryType::Amulet)
            {
                amulet = jewelleryPiece;
            }
            else if (rightRing)
            {
                leftRing = jewelleryPiece;
            }
            else
            {
                rightRing = jewelleryPiece;
            }
        }
    }
}

void EquippedJewellery::jewelleryChange(const Wt::Dbo::ptr<Jewellery>& jewelleryPiece)
{
    if (not jewelleryPiece->isEquipped())
    {
        hadleUnequip(jewelleryPiece);
        return;
    }

    hadleEquip(jewelleryPiece);
}

void EquippedJewellery::hadleUnequip(const Wt::Dbo::ptr<Jewellery>& jewelleryPiece)
{
    if (jewelleryPiece->type() == JewelleryType::Amulet and jewelleryPiece == amulet)
    {
        amulet = nullptr;
        return;
    }
    if (jewelleryPiece->type() == JewelleryType::Ring)
    {
        if (jewelleryPiece == leftRing)
        {
            leftRing = nullptr;
            return;
        }

        if (jewelleryPiece == rightRing)
        {
            rightRing = nullptr;
            return;
        }
    }

    throw std::runtime_error("This shouldn't happend");
}

void EquippedJewellery::hadleEquip(const Wt::Dbo::ptr<Jewellery>& jewelleryPiece)
{
    if (jewelleryPiece->type() == JewelleryType::Amulet)
    {
        if (amulet)
        {
            amulet.modify()->flipEquippedState();
        }

        amulet = jewelleryPiece;
    }
    else
    {
        if (rightRing && leftRing)
        {
            leftRing.modify()->flipEquippedState();
            leftRing = rightRing;
        }
        else if (rightRing)
        {
            leftRing = rightRing;
        }
        rightRing = jewelleryPiece;
    }
}

}
