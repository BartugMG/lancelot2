#include "CharacterCardWidget.hpp"
#include <Wt/WHBoxLayout.h>
#include <Wt/WVBoxLayout.h>
#include <Wt/WBreak.h>
#include <Wt/WLineEdit.h>
#include <Wt/WSpinBox.h>
#include <Wt/WSplitButton.h>
#include <Wt/WPopupMenu.h>
#include <Wt/WPushButton.h>
#include <Wt/WTabWidget.h>
#include "LancelotApp.hpp"
#include "User.hpp"
#include "Character.hpp"
#include "Equipment.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "GenericItemWidget.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"
#include "StatusChangeWidget.hpp"
#include "Pouch.hpp"
#include "GeneralInfoWidget.hpp"
#include "ParametersWidget.hpp"
#include "StatusPanelWidget.hpp"
#include "PouchWidget.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "EquipmentWidget.hpp"
#include "CommonModelsFillers.hpp"
#include "Race.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "NewItemDialog.hpp"
#include "NewStatusChangeDialog.hpp"
#include "TurnEndDialog.hpp"
#include "ExpGainDialog.hpp"
#include "PouchChangeDialog.hpp"

namespace Lancelot
{
using namespace Wt;
using namespace std::string_literals;

CharacterCardWidget::CharacterCardWidget()
{
    setStyleClass("jumbotron");

    m_mainLayout = setLayout(std::make_unique<WVBoxLayout>());

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);
    m_character = session.user()->m_characters.front();

    auto name = m_mainLayout->addWidget(std::make_unique<WText>());
    name->setText(WString("<h2 style=\"text-align:center\">{1}</h2>").arg(m_character->name()));

    auto nextTurnButton = m_mainLayout->addWidget(std::make_unique<WPushButton>("KONIEC TURY"));
    nextTurnButton->setMargin(75, Side::Left);
    nextTurnButton->setMargin(75, Side::Right);
    nextTurnButton->clicked().connect(this, &CharacterCardWidget::showTurnEndDialog);

    auto tabWidget = m_mainLayout->addWidget(std::make_unique<WTabWidget>());

    addFeaturesTab(tabWidget);
    addEquipmentTab(tabWidget);
}

void CharacterCardWidget::addFeaturesTab(Wt::WTabWidget* tabWidget)
{
    auto infoContainer = std::make_unique<WContainerWidget>();
    auto infoContainerLayout = infoContainer->setLayout(std::make_unique<WVBoxLayout>());
    infoContainerLayout->setContentsMargins(0, 0, 0, 0);
    m_generalInfo = infoContainerLayout->addWidget(std::make_unique<GeneralInfoWidget>(m_character));
    m_parameters = infoContainerLayout->addWidget(std::make_unique<ParametersWidget>(
            m_character->parametersWithChangesAndEq()));
    m_changes = infoContainerLayout->addWidget(std::make_unique<StatusPanelWidget>());
    m_changes->changed.connect([&]() { m_changes->update(m_character); });
    auto buttonContainer = infoContainerLayout ->addWidget(std::make_unique<WContainerWidget>());
    auto newStatChangeButton = buttonContainer->addWidget(std::make_unique<WPushButton>("Nowa zmiana statusu"));
    newStatChangeButton->clicked().connect(this, &CharacterCardWidget::showNewStatChangeDialog);
    auto addExpButton = buttonContainer->addWidget(std::make_unique<WPushButton>("Dodaj punkty doświadczenia"));
    addExpButton->clicked().connect(this, &CharacterCardWidget::showAddExpDialog);

    tabWidget->addTab(std::move(infoContainer), "Cechy postaci");
}

void CharacterCardWidget::addEquipmentTab(Wt::WTabWidget* tabWidget)
{
    auto equipmentContainer = std::make_unique<WContainerWidget>();
    auto equipmentContainerLayout = equipmentContainer->setLayout(std::make_unique<WVBoxLayout>());
    equipmentContainerLayout->setContentsMargins(0, 0, 0, 0);
    m_eq = equipmentContainerLayout->addWidget(std::make_unique<EquipmentWidget>(m_character->equipment));
    m_eq->equipmentChanged.connect(this, &CharacterCardWidget::updateAfterEqOrNewStatChange);
    m_pouch = equipmentContainerLayout->addWidget(std::make_unique<PouchWidget>(m_character->equipment->pouch));
    auto buttonContainer = equipmentContainerLayout->addWidget(std::make_unique<WContainerWidget>());
    auto newItemButton = buttonContainer->addWidget(std::make_unique<WPushButton>("Nowy przedmiot"));
    auto pouchChangeButton = buttonContainer->addWidget(std::make_unique<WPushButton>("Zmiana w sakiewce"));

    auto equipment = m_character->equipment;
    newItemButton->clicked().connect(this, &CharacterCardWidget::showNewItemDialog);

    pouchChangeButton->clicked().connect(this, &CharacterCardWidget::showPouchChangeDialog);

    tabWidget->addTab(std::move(equipmentContainer), "Ekwipunek");

    m_eq->potionUsed.connect([this](Dbo::ptr<StatusChange> sc){
        if (m_character.modify()->applyPotion(sc))
        {
            updateAfterEqOrNewStatChange();
        }
        else
        {
            WMessageBox::show("Operacja niedozwolona", "<p>Już użyto mikstury w tej turze!</p>",
                    StandardButton::Ok);
        }
    });
}

void CharacterCardWidget::showNewItemDialog()
{
    Dbo::Transaction t(LancelotApp::session());
    auto dialog = addChild(std::make_unique<NewItemDialog>(m_character->equipment));
    dialog->itemCreated.connect([this](){ m_eq->update(); });
    dialog->show();
}

void CharacterCardWidget::showPouchChangeDialog()
{
    Dbo::Transaction t(LancelotApp::session());
    auto dialog = addChild(std::make_unique<PouchChangeDialog>());
    dialog->pouchChanged.connect([&](PouchDelta delta){
        Dbo::Transaction t2(LancelotApp::session());
        m_character->equipment->pouch.modify()->applyDelta(delta);
        m_pouch->update();
    });
    dialog->show();
}

void CharacterCardWidget::showNewStatChangeDialog()
{
    Dbo::Transaction t(LancelotApp::session());
    auto dialog = addChild(std::make_unique<NewStatusChangeDialog>(m_character));
    dialog->statusChangeCreated.connect([this](){ updateAfterEqOrNewStatChange(); });
    dialog->show();
}

void CharacterCardWidget::showTurnEndDialog()
{
    Dbo::Transaction t(LancelotApp::session());
    auto dialog = addChild(std::make_unique<TurnEndDialog>());
    dialog->turnEnded.connect([this](const LifeParameters& paramLoss){
        Dbo::Transaction t2(LancelotApp::session());
        m_character.modify()->evaluateNextTurn(paramLoss);
        updateAfterEqOrNewStatChange();
    });
    dialog->show();
}

void CharacterCardWidget::showAddExpDialog()
{
    Dbo::Transaction t(LancelotApp::session());
    auto dialog = addChild(std::make_unique<ExpGainDialog>());
    dialog->expAdded.connect([&](int amount){
        Dbo::Transaction t2(LancelotApp::session());
        m_character.modify()->addExp(amount);
        m_generalInfo->update();
        update();
    });
    dialog->show();
}

void CharacterCardWidget::update()
{
    Dbo::Transaction t(LancelotApp::session());
    m_generalInfo->update();
    m_changes->update(m_character);
    m_parameters->update();
    m_eq->update();
    m_pouch->update();
}

void CharacterCardWidget::updateAfterEqOrNewStatChange()
{
    Dbo::Transaction t(LancelotApp::session());
    m_changes->update(m_character);
    m_parameters->update(m_character->parametersWithChangesAndEq());
}

void CharacterCardWidget::initialCharacterConfig()
{
    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);
    auto character = session.user()->m_characters.front();
    if (character->equipment->items.empty())
    {
        auto item = session.add(std::make_unique<Item>("Błoto"s, "Kiepsko pachnie"s, 1, 3));
        character->equipment.modify()->items.insert(item);

        auto item2 = session.add(std::make_unique<Item>("Słoik z marmoladą"s, "Dyniowa, mniam"s, 1, 1));
        character->equipment.modify()->items.insert(item2);

        log("notice") << "Added mud to equipment";
    }
    if (character->equipment->weapons.empty())
    {
        auto weapon = session.add(std::make_unique<Weapon>(
                "Mieczor"s, "Fajny ładny"s, 1, 8,
                WeaponType::Sword, Dice{1, 6}, 1, boost::none));
        character->equipment.modify()->weapons.insert(weapon);

        log("notice") << "Added sword to equipment";
    }
    if (character->equipment->potions.empty())
    {
        auto potion = session.add(std::make_unique<Potion>("Mikstura kocich oczu"s,
                "Widzenie w ciemności"s, 2, 1));

        auto change = session.add(std::make_unique<StatusChange>("Widzenie w ciemności"s,
                "[Mikstura kocich oczu]"s, 2));
        auto param = session.add(std::make_unique<ParameterModel>(Parameter::Agility, 2));

        change.modify()->params.insert(param);
        change.modify()->potion = potion;

        character->equipment.modify()->potions.insert(potion);

        log("notice") << "Added potion to equipment";
    }
    if (character->equipment->armor.empty())
    {
        auto armor = session.add(std::make_unique<Armor>("Skórzana zbroja"s, "Taka tam zwykła"s, 1, 10,
                ArmorType::Full, ArmorClass::Light, -1));

        auto change = session.add(std::make_unique<StatusChange>("Ćwieki"s,
                                                                 "[Skórzana zbroja]"s));
        auto param = session.add(std::make_unique<ParameterModel>(Parameter::Charisma, -1));
        change.modify()->params.insert(param);
        change.modify()->armor = armor;

        character->equipment.modify()->armor.insert(armor);

        log("notice") << "Added armor to equipment";
    }
    if (character->equipment->jewellery.empty())
    {
        auto ring = session.add(std::make_unique<Jewellery>("Pierścień siły"s, "Wzmacnia siłę"s,
                1, 0.2, JewelleryType::Ring));

        auto change = session.add(std::make_unique<StatusChange>("Wzmocnienie siły"s,
                                                                 "[Pierścień siły]"s));
        auto param = session.add(std::make_unique<ParameterModel>(Parameter::Strength, 1));
        change.modify()->params.insert(param);
        change.modify()->jewellery = ring;

        character->equipment.modify()->jewellery.insert(ring);

        log("notice") << "Added ring to equipment";
    }
}

}
