#pragma once

#include <Wt/WPushButton.h>
#include "StatusChange.hpp"

namespace Lancelot
{

class StatusChangeWidget : public Wt::WPushButton
{
public:
    explicit StatusChangeWidget(Wt::Dbo::ptr<StatusChange> statusChange);
    Wt::Signal<> removed;

private:
    Wt::Dbo::ptr<StatusChange> m_statusChange;

    void showDescription();
    void showMsgBox(const Wt::WString& description);
};

}
