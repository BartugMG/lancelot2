#include "NewStatusChangeDialog.hpp"
#include "StatusChangeForm.hpp"
#include <Wt/WPushButton.h>

namespace Lancelot
{
using namespace Wt;

NewStatusChangeDialog::NewStatusChangeDialog(Wt::Dbo::ptr<Character> character):
    WDialog("Nowa zmiana statusu"),
    m_character(std::move(character))
{
    rejectWhenEscapePressed();
    addStyleClass("modal-diff");
    contents()->addStyleClass("modal-body-diff");

    m_statusChangeForm = contents()->addWidget(std::make_unique<StatusChangeForm>(true, NameEditor::Existing));

    auto cancel = footer()->addWidget(std::make_unique<WPushButton>("Anuluj"));
    cancel->clicked().connect(this, &NewStatusChangeDialog::reject);

    auto ok = footer()->addWidget(std::make_unique<WPushButton>("OK"));
    ok->clicked().connect(this, &NewStatusChangeDialog::createFromView);
}

void NewStatusChangeDialog::createFromView()
{
    if (not m_statusChangeForm->validate())
    {
        StatusChangeForm::showValidationError();
        return;
    }

    auto statChange = m_statusChangeForm->create();
    m_character.modify()->changes.insert(statChange);

    statusChangeCreated.emit();
    accept();
}

}
