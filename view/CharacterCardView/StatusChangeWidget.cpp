#include "StatusChangeWidget.hpp"
#include <Wt/WMessageBox.h>
#include "ParameterModel.hpp"
#include "LancelotApp.hpp"

namespace Lancelot
{
using namespace Wt;

StatusChangeWidget::StatusChangeWidget(Wt::Dbo::ptr<StatusChange> statusChange):
    m_statusChange(std::move(statusChange))
{
    setText(m_statusChange->name());
    clicked().connect(this, &StatusChangeWidget::showDescription);
}

void StatusChangeWidget::showDescription()
{
    WString description;

    if(auto duration = m_statusChange->duration(); duration.is_initialized())
    {
        description += WString("<p>Pozostało: {1} tur</p>").arg(*duration);
    }

    {
        Dbo::Transaction t(LancelotApp::session());
        for (auto it = m_statusChange->params.begin(); it != m_statusChange->params.end(); ++it)
        {
            const auto[param, value] = (*it)->typeToValue;
            description += WString("<p>{1}: {2}</p>").arg(toString(param)).arg(value);
        }

        if(m_statusChange->duration().is_initialized() or
           not m_statusChange->params.empty())
        {
            description += WString("<br/>");
        }

    }

    description += WString("<p>{1}</p>").arg(m_statusChange->description());

    showMsgBox(description);
}

void StatusChangeWidget::showMsgBox(const WString& description)
{
    auto msgBox = std::make_unique<WMessageBox>(m_statusChange->name(),
                                                description, Icon::Information, Wt::StandardButton::Ok);

    msgBox->button(StandardButton::Ok)->clicked().connect([&](){ msgBox->accept(); });

    if (m_statusChange->duration().is_initialized())
    {
        auto button = msgBox->addButton("Usuń", StandardButton::Abort);
        button->clicked().connect([&]() {

            auto result = Wt::WMessageBox::show("Potwierdzenie",
                                                "<p>Na pewno chcesz usunąć tę zmianę statusu?</p>",
                                                Wt::StandardButton::Yes | Wt::StandardButton::No);

            if (result == Wt::StandardButton::Yes)
            {
                msgBox->accept();
                Wt::Dbo::Transaction t(LancelotApp::session());
                m_statusChange.remove();
                removeFromParent();
            }

        });
    }

    msgBox->exec();

}

}
