#include "ExpGainDialog.hpp"
#include <Wt/WPushButton.h>
#include <Wt/WLabel.h>
#include "Utility.hpp"

namespace Lancelot
{
using namespace Wt;

ExpGainDialog::ExpGainDialog():
    WDialog("Uzyskane doświadczenie")
{
    rejectWhenEscapePressed();
    addStyleClass("modal-diff");
    contents()->addStyleClass("modal-body-diff");

    m_expBox = Utility::addFormToContainerWidget<WSpinBox>("Uzyskane doświadczenie:", contents());
    m_expBox->setRange(0, 100);

    auto cancel = footer()->addWidget(std::make_unique<WPushButton>("Anuluj"));
    cancel->clicked().connect(this, &ExpGainDialog::reject);

    auto ok = footer()->addWidget(std::make_unique<WPushButton>("OK"));
    ok->clicked().connect(this, [&](){
        if (m_expBox->validate() == ValidationState::Valid)
        {
            expAdded.emit(m_expBox->value());
            accept();
        }
    });
}

}
