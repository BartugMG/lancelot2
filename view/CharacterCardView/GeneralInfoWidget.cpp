#include "GeneralInfoWidget.hpp"
#include <Wt/WVBoxLayout.h>
#include <Wt/WGridLayout.h>
#include <Wt/WText.h>
#include <Wt/WPanel.h>
#include <Wt/WRegExpValidator.h>
#include "Character.hpp"
#include "LancelotApp.hpp"
#include "Race.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"

namespace Lancelot
{
using namespace Wt;
using namespace std::string_literals;

GeneralInfoWidget::GeneralInfoWidget(Wt::Dbo::ptr<Character> character):
    m_character(std::move(character))
{
    setTitle("Informacje ogólne");
    setCollapsible(true);
    addForms(static_cast<bool>(m_character));
}

void GeneralInfoWidget::addForms(bool isCharacterSet)
{
    auto innerContainer = setCentralWidget(std::make_unique<WContainerWidget>());
    auto innerLayout = innerContainer->setLayout(std::make_unique<WGridLayout>());

    if (not isCharacterSet)
    {
        m_name = Utility::addFormToGridLayout<WLineEdit>(innerLayout, "Imię:"s, 0, 0, 4, isCharacterSet);
        m_name->setValidator(std::make_unique<WRegExpValidator>(Utility::NAME_REGEX_NONUMBER));
        m_name->setTextSize(30);
        m_name->setMaxLength(30);

        m_nameIndicator = innerLayout->addWidget(std::make_unique<WText>(),
                1, 0, 0, 4, Wt::AlignmentFlag::Left);

        m_name->changed().connect(this, &GeneralInfoWidget::validateName);
    }

    m_race = Utility::addFormToGridLayout<WComboBox>(innerLayout, "Rasa:"s, 2, 0, 4, isCharacterSet);
    for(auto race: RACES)
    {
        m_race->insertItem(race, toString(race));
    }
    m_race->setDisabled(isCharacterSet);

    m_techClass = Utility::addFormToGridLayout<WComboBox>(innerLayout, "Klasa techniczna:"s, 3, 0, 2,
                                                          isCharacterSet);
    for(auto techClass: TECH_CLASSES)
    {
        m_techClass->insertItem(techClass, toString(techClass));
    }
    m_techClass->setDisabled(isCharacterSet);

    m_mageClass = Utility::addFormToGridLayout<WComboBox>(innerLayout, "Klasa magiczna:"s, 3, 2, 2,
                                                          isCharacterSet);
    for(auto mageClass: MAGE_CLASSES)
    {
       m_mageClass->insertItem(mageClass, toString(mageClass));
    }
    m_mageClass->setDisabled(isCharacterSet);

    m_deity = Utility::addFormToGridLayout<WComboBox>(innerLayout, "Bóstwo:"s, 4, 0, 2, isCharacterSet);
    for(auto deity: DEITIES)
    {
        m_deity->insertItem(static_cast<int>(deity), toString(deity));
    }
    m_deity->setDisabled(isCharacterSet);

    m_karma = Utility::addFormToGridLayout<WSpinBox>(innerLayout, "Karma:"s, 4, 2, 2, isCharacterSet);
    m_karma->setRange(0, 100);
    m_karma->setTextSize(5);
    m_level = Utility::addFormToGridLayout<WSpinBox>(innerLayout, "Poziom:"s, 5, 0, 2, isCharacterSet);
    m_level->setRange(1, 100);
    m_level->setTextSize(5);
    m_exp = Utility::addFormToGridLayout<WSpinBox>(innerLayout, "Doświadcznie:"s, 5, 2, 2, isCharacterSet);
    m_exp->setRange(0, 99);
    m_exp->setTextSize(5);
}

void GeneralInfoWidget::update()
{
    if (m_character)
    {
        updateFromExistingCharacter();
    }
    else
    {
        updateForNewCharacter();
    }
}

void GeneralInfoWidget::updateFromExistingCharacter()
{
    const auto raceId = m_character->race->asEnum();
    m_race->setCurrentIndex(raceId);

    const auto techClassId = m_character->techClass->asEnum();
    m_techClass->setCurrentIndex(techClassId);

    const auto mageClassId = m_character->mageClass->asEnum();
    m_mageClass->setCurrentIndex(mageClassId);

    const auto deity = m_character->deity();
    m_deity->setCurrentIndex(static_cast<int>(deity));

    m_karma->setValue(m_character->karma());

    m_level->setValue(m_character->level());
    m_exp->setValue(m_character->exp());

}

void GeneralInfoWidget::updateForNewCharacter()
{
    m_level->setValue(1);
    m_karma->setValue(100);
}

void GeneralInfoWidget::validateName()
{
    if (m_name->validate() != ValidationState::Valid or
        m_name->text().empty())
    {
        m_nameIndicator->setText("<b><font color=\"red\">Imię nieprawidłowe</font></b>");
    }
    else
    {
        m_nameIndicator->setText("");
    }
}

}
