#pragma once

#include <Wt/WDialog.h>
#include <Wt/WContainerWidget.h>
#include <Wt/Dbo/ptr.h>
#include "Parameter.hpp"
#include "Equipment.hpp"
#include "StatusChangeForm.hpp"

namespace Lancelot
{

class ItemForm: public Wt::WContainerWidget
{
public:
    ItemForm();
    virtual bool create(const Wt::Dbo::ptr<Equipment>& eq) const;

protected:
    virtual bool validate() const;
    bool validateForm(Wt::WAbstractSpinBox* form, const Wt::WString& fieldName) const;

    Wt::WLineEdit* m_name;
    Wt::WTextArea* m_desctiption;
    Wt::WSpinBox* m_amount;
    Wt::WDoubleSpinBox* m_singularWeight;

};

/////////////////////////////////////////////////////////////////////////////////////

class PotionForm: public ItemForm
{
public:
    PotionForm();
    bool create(const Wt::Dbo::ptr<Equipment>& eq) const override;

private:
    bool validate() const override;

    StatusChangeForm* m_status;

};

/////////////////////////////////////////////////////////////////////////////////////

class JewelleryForm: public ItemForm
{
public:
    JewelleryForm();
    bool create(const Wt::Dbo::ptr<Equipment>& eq) const override;

private:
    bool validate() const override;

    Wt::WComboBox* m_type;
    StatusChangeForm* m_status;

};

/////////////////////////////////////////////////////////////////////////////////////

class ArmorForm: public ItemForm
{
public:
    ArmorForm();
    bool create(const Wt::Dbo::ptr<Equipment>& eq) const override;

private:
    bool validate() const override;

    Wt::WComboBox* m_type;
    Wt::WComboBox* m_aclass;
    Wt::WSpinBox* m_reduction;
    StatusChangeForm* m_status;

};

/////////////////////////////////////////////////////////////////////////////////////

class WeaponForm: public ItemForm
{
public:
    WeaponForm();
    bool create(const Wt::Dbo::ptr<Equipment>& eq) const override;

private:
    bool validate() const override;
    void prepareDiceForm();
    void prepareDurabilityForm();

    Wt::WComboBox* m_type;
    Wt::WSpinBox* m_dices;
    Wt::WSpinBox* m_sides;
    Wt::WSpinBox* m_modifier;
    Wt::WSpinBox* m_durability;
};

/////////////////////////////////////////////////////////////////////////////////////

class NewItemDialog : public Wt::WDialog
{
public:
    explicit NewItemDialog(Wt::Dbo::ptr<Equipment> eq);

    Wt::Signal<> itemCreated;

private:
    void createComboBox();
    void createStack();

    void createFromView();

    Wt::Dbo::ptr<Equipment> m_eq;
    Wt::WStackedWidget* m_stack;
    std::array<ItemForm*, 5> m_forms;

};

/////////////////////////////////////////////////////////////////////////////////////

}
