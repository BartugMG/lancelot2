#include "ParametersWidget.hpp"
#include <Wt/WContainerWidget.h>
#include <Wt/WGridLayout.h>
#include <Wt/WSpinBox.h>

namespace Lancelot
{
using namespace Wt;
using namespace std::string_literals;

ParametersWidget::ParametersWidget(std::optional<Parameters> params):
    m_params(std::move(params))
{
    setTitle("Parametry postaci");
    setCollapsible(true);
    addForms();
}

void ParametersWidget::addForms()
{
    using namespace Utility;
    auto innerContainer = setCentralWidget(std::make_unique<WContainerWidget>());
    auto innerLayout = innerContainer->setLayout(std::make_unique<WGridLayout>());

    m_health = addFormToGridLayout<WSpinBox>(innerLayout, "PZ:"s, 5, 0, 0, m_params.has_value());
    m_stamina = addFormToGridLayout<WSpinBox>(innerLayout, "PK:"s, 5, 1, 0, m_params.has_value());
    m_mana = addFormToGridLayout<WSpinBox>(innerLayout, "PM:"s, 5, 2, 0, m_params.has_value());

    m_strength = addFormToGridLayout<WSpinBox>(innerLayout, "Siła:"s, 6, 0, 0, m_params.has_value());
    m_agility = addFormToGridLayout<WSpinBox>(innerLayout, "Zrę:"s, 6, 1, 0, m_params.has_value());
    m_intelligence = addFormToGridLayout<WSpinBox>(innerLayout, "Int:"s, 6, 2, 0, m_params.has_value());
    m_charisma = addFormToGridLayout<WSpinBox>(innerLayout, "Char:"s, 6, 3, 0, m_params.has_value());

    m_techAbility = addFormToGridLayout<WSpinBox>(innerLayout, "ZT:"s, 7, 0, 0, true);
    m_mageAbility = addFormToGridLayout<WSpinBox>(innerLayout, "ZM:"s, 7, 2, 0, true);
}

void ParametersWidget::update()
{
    if (m_params.has_value())
    {
        m_health->setValue(m_params->health());
        m_mana->setValue(m_params->mana());
        m_stamina->setValue(m_params->stamina());
        m_strength->setValue(m_params->strength());
        m_agility->setValue(m_params->agility());
        m_intelligence->setValue(m_params->intelligence());
        m_charisma->setValue(m_params->charisma());
        m_techAbility->setValue(m_params->techAbility());
        m_mageAbility->setValue(m_params->magicAbility());
    }
}

void ParametersWidget::update(const Parameters& params)
{
    m_params = params;
    update();
}

Parameters ParametersWidget::getParamsFromWidgetValues() const
{
    Parameters params;
    params.health() = m_health->value();
    params.mana() = m_mana->value();
    params.stamina() = m_stamina->value();
    params.strength() = m_strength->value();
    params.agility() = m_agility->value();
    params.intelligence() = m_intelligence->value();
    params.charisma() = m_charisma->value();
    params.techAbility() = m_techAbility->value();
    params.magicAbility() = m_mageAbility->value();
    return params;
}

}
