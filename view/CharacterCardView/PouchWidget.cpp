#include "PouchWidget.hpp"
#include <Wt/WHBoxLayout.h>
#include <Wt/WTemplate.h>
#include <Wt/WSpinBox.h>

namespace Lancelot
{
using namespace Wt;

PouchWidget::PouchWidget(Wt::Dbo::ptr<Pouch> pouch):
    m_pouch(std::move(pouch))
{
    setTitle("Sakiewka");
    setCollapsible(true);
    addForms();
}

void PouchWidget::addForms()
{
    auto innerContainer = setCentralWidget(std::make_unique<WContainerWidget>());
    auto laylout = innerContainer->setLayout(std::make_unique<WHBoxLayout>());

    for(auto coinType: {CoinType::Platinum, CoinType::Gold,
                        CoinType::Silver, CoinType::Copper})
    {
        auto form = laylout->addWidget(
                std::make_unique<Wt::WTemplate>(Wt::WString::tr("lineEdit-template")));
        form->addFunction("id", &Wt::WTemplate::Functions::id);

        form->bindString("label", toString(coinType));
        auto spinbox = form->bindWidget("edit", std::make_unique<WSpinBox>());
        spinbox ->setReadOnly(true);
        m_forms.emplace(coinType, spinbox);
    }

}


void PouchWidget::update()
{
    for(auto& [coinType, form] : m_forms)
    {
        form->setValue(m_pouch->coins(coinType));
    }
}

}
