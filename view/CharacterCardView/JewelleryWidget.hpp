#pragma once

#include "Jewellery.hpp"
#include "GenericItemWidget.hpp"


namespace Lancelot
{

class JewelleryWidget : public GenericItemWidget<Jewellery>
{
public:
    explicit JewelleryWidget(Wt::Dbo::ptr<Jewellery> jewellery);
    Wt::Signal<Wt::Dbo::ptr<Jewellery>>& equiped()
    { return m_equiped; }

private:
    Wt::Signal<Wt::Dbo::ptr<Jewellery>> m_equiped;
    void showJewelleryDesctiption();
    Wt::WString createDescriptionContent();
    void addJewelleryWearingItem();

};

}
