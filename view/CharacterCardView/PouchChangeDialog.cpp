#include "PouchChangeDialog.hpp"
#include "Utility.hpp"
#include <Wt/WPushButton.h>
#include <Wt/WLabel.h>

namespace Lancelot
{
using namespace Wt;

PouchChangeDialog::PouchChangeDialog():
    WDialog("Zmiany w sakiewce")
{
    rejectWhenEscapePressed();
    addStyleClass("modal-diff");
    contents()->addStyleClass("modal-body-diff");

    m_platBox = Utility::addFormToContainerWidget<WSpinBox>("Zmiana platyn:", contents());
    m_platBox->setRange(-100, 100);
    m_goldBox = Utility::addFormToContainerWidget<WSpinBox>("Zmiana złota:", contents());
    m_goldBox->setRange(-10, 10);
    m_silverBox = Utility::addFormToContainerWidget<WSpinBox>("Zmiana srebra:", contents());
    m_silverBox->setRange(-10, 10);
    m_bronzeBox = Utility::addFormToContainerWidget<WSpinBox>("Zmiana miedzi:", contents());
    m_bronzeBox->setRange(-10, 10);

    auto cancel = footer()->addWidget(std::make_unique<WPushButton>("Anuluj"));
    cancel->clicked().connect(this, &PouchChangeDialog::reject);

    auto ok = footer()->addWidget(std::make_unique<WPushButton>("OK"));
    ok->clicked().connect(this, [&](){
        if (validateAll())
        {
            pouchChanged.emit(fetchValuesFromWidgets());
            accept();
        }
    });

}

bool PouchChangeDialog::validateAll() const
{
    auto validState = ValidationState::Valid;
    return m_platBox->validate() == validState &&
           m_goldBox->validate() == validState &&
           m_silverBox->validate() == validState &&
           m_bronzeBox->validate() == validState;
}

PouchDelta PouchChangeDialog::fetchValuesFromWidgets() const
{
    return PouchDelta{m_platBox->value(),
                      m_goldBox->value(),
                      m_silverBox->value(),
                      m_bronzeBox->value()};
}

}
