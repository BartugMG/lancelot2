#pragma once

#include <Wt/WContainerWidget.h>
#include <Wt/WPanel.h>
#include <Wt/Dbo/ptr.h>
#include <Pouch.hpp>

namespace Lancelot
{
class Pouch;

class PouchWidget : public Wt::WPanel
{
public:
    explicit PouchWidget(Wt::Dbo::ptr<Pouch> pouch);
    void update();

private:
    void addForms();

    Wt::Dbo::ptr<Pouch> m_pouch;
    std::map<CoinType, Wt::WSpinBox*> m_forms;

};

}
