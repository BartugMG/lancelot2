#!/bin/bash

if [ -d "wt" ]; then
    echo "Found cached Wt"
    cd wt/build
    make install
else
    echo "Cached Wt not found, proceed to compilation"
    git clone https://github.com/emweb/wt.git
    cd wt
    mkdir build
    cd build
    cmake ..
    make -j4
    make install
fi
cd ../..