#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <Wt/Dbo/ptr.h>
#include "CommonEnums.hpp"

namespace Lancelot
{
class StatusChange;
class TechClass;
class MageClass;
class Character;

class Race
{
public:
    Race();

    explicit Race(RaceEnum raceId);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_raceId, "race_enum"s);

        Wt::Dbo::hasMany(a, changes, Wt::Dbo::ManyToOne, "race"s);

        Wt::Dbo::hasMany(a, techClasses, Wt::Dbo::ManyToMany, "race_techclass"s);
        Wt::Dbo::hasMany(a, mageClasses, Wt::Dbo::ManyToMany, "race_mageclass"s);
        Wt::Dbo::hasMany(a, m_chars, Wt::Dbo::ManyToOne, "race"s);
    }

    Wt::Dbo::collection<Wt::Dbo::ptr<StatusChange>> changes;
    Wt::Dbo::collection<Wt::Dbo::ptr<TechClass>> techClasses;
    Wt::Dbo::collection<Wt::Dbo::ptr<MageClass>> mageClasses;

    RaceEnum asEnum() const
    { return m_raceId; }

private:
    RaceEnum m_raceId;
    Wt::Dbo::collection<Wt::Dbo::ptr<Character>> m_chars;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::Race)