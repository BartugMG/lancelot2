#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <Wt/Dbo/ptr.h>
#include "Pouch.hpp"


namespace Lancelot
{
using namespace std::string_literals;

class Character;
class Item;
class Weapon;
class Pouch;
class Potion;
class Armor;
class Jewellery;

class Equipment
{
public:

    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;
        Wt::Dbo::belongsTo(a, m_character, "character"s);
        Wt::Dbo::hasMany(a, items, Wt::Dbo::ManyToOne, "equipment"s);
        Wt::Dbo::hasMany(a, weapons, Wt::Dbo::ManyToOne, "equipment"s);
        Wt::Dbo::hasMany(a, armor, Wt::Dbo::ManyToOne, "equipment"s);
        Wt::Dbo::hasMany(a, jewellery, Wt::Dbo::ManyToOne, "equipment"s);
        Wt::Dbo::hasMany(a, potions, Wt::Dbo::ManyToOne, "equipment"s);
        Wt::Dbo::hasOne(a, pouch);
    }

    Wt::Dbo::collection<Wt::Dbo::ptr<Item>> items;
    Wt::Dbo::collection<Wt::Dbo::ptr<Weapon>> weapons;
    Wt::Dbo::collection<Wt::Dbo::ptr<Armor>> armor;
    Wt::Dbo::collection<Wt::Dbo::ptr<Jewellery>> jewellery;
    Wt::Dbo::collection<Wt::Dbo::ptr<Potion>> potions;
    Wt::Dbo::weak_ptr<Pouch> pouch;

private:
    Wt::Dbo::ptr<Character> m_character;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::Equipment)
