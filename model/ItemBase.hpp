#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <Wt/Dbo/ptr.h>

namespace Lancelot
{

class ItemBase
{
public:
    ItemBase(std::string name, std::string description,
             int amount, double singularWeight):
        m_name(std::move(name)),
        m_description(std::move(description)),
        m_amount(amount),
        m_singularWeight(singularWeight)
    {}

    const std::string& name() const
    { return m_name; }

    const std::string& description() const
    { return m_description; }

    int amount() const
    { return m_amount; }

    void setAmount(int amount)
    { m_amount = amount; }

    double weight() const
    { return m_singularWeight * m_amount; }

protected:
    std::string m_name;
    std::string m_description;
    int m_amount;
    double m_singularWeight;
};

}
