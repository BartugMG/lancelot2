#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <boost/optional.hpp>

namespace Lancelot
{
class Character;
class ParameterModel;
class Potion;
class Armor;
class Jewellery;
class Race;

class StatusChange
{
public:
    StatusChange();

    StatusChange(std::string name, std::string description,
                 boost::optional<int> duration = boost::none);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_description, "description"s);
        Wt::Dbo::field(a, m_duration, "duration"s);

        Wt::Dbo::belongsTo(a, potion, "potion"s, Wt::Dbo::OnDeleteCascade);
        Wt::Dbo::belongsTo(a, armor, "armor"s, Wt::Dbo::OnDeleteCascade);
        Wt::Dbo::belongsTo(a, jewellery, "jewellery"s, Wt::Dbo::OnDeleteCascade);
        Wt::Dbo::belongsTo(a, race, "race"s, Wt::Dbo::OnDeleteCascade);
        Wt::Dbo::hasMany(a, params, Wt::Dbo::ManyToOne, "status_change"s);
        Wt::Dbo::hasMany(a, m_characters, Wt::Dbo::ManyToMany, "character_changes"s);
    }

    Wt::Dbo::collection<Wt::Dbo::ptr<ParameterModel>> params;
    Wt::Dbo::ptr<Potion> potion;
    Wt::Dbo::ptr<Armor> armor;
    Wt::Dbo::ptr<Jewellery> jewellery;
    Wt::Dbo::ptr<Race> race;

    const std::string& name() const
    { return m_name; }

    const std::string& description() const
    { return m_description; }

    const boost::optional<int>& duration() const
    { return m_duration; }

    void decrementDuration()
    { --m_duration.value(); }

private:
    std::string m_name;
    std::string m_description;
    boost::optional<int> m_duration;
    Wt::Dbo::collection<Wt::Dbo::ptr<Character>> m_characters;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::StatusChange)