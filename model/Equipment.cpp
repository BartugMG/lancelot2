#include "Equipment.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Character.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "Pouch.hpp"
#include "Potion.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "User.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"

DBO_INSTANTIATE_TEMPLATES(Lancelot::Equipment)
