#include "Armor.hpp"
#include <Wt/Dbo/Impl.h>
#include "Equipment.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"
#include "Character.hpp"
#include "Potion.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "User.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "Item.hpp"
#include "Weapon.hpp"

namespace Lancelot
{

Armor::Armor(std::string name, std::string description, int amount, double singularWeight, ArmorType type,
             ArmorClass aclass, int damageReduction):
    ItemBase(std::move(name), std::move(description),
             amount, singularWeight),
    m_type(type),
    m_class(aclass),
    m_damageReduction(damageReduction),
    m_isEquipped(false)
{}

Armor::Armor():
        Armor(""s, ""s, 0, 0, ArmorType::Full, ArmorClass::None, 0)
{}

void Armor::flipEquippedState()
{
    m_isEquipped = !m_isEquipped;
}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::Armor)

