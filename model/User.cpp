#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "User.hpp"
#include "Character.hpp"
#include "ParameterPack.hpp"
#include "Equipment.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"
#include "Pouch.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"

DBO_INSTANTIATE_TEMPLATES(Lancelot::User)
