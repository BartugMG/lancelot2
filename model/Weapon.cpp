#include "Weapon.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Equipment.hpp"
#include <string>

namespace Lancelot
{
using namespace std::string_literals;

Weapon::Weapon():
        Weapon(""s, ""s, 0, 0, WeaponType::Other, Dice{0, 0}, 0, boost::none)
{}

Weapon::Weapon(std::string name, std::string description, int amount, double singularWeight,
               WeaponType type, Dice dice, int modifier, boost::optional<int> durability):
    ItemBase(std::move(name), std::move(description), amount, singularWeight),
    m_type(type),
    m_dice(dice),
    m_modifier(modifier),
    m_durability(durability),
    m_isEquipped(false)
{}

void Weapon::flipEquippedState()
{
    m_isEquipped = !m_isEquipped;
}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::Weapon)
