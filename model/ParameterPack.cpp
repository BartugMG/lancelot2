#include "ParameterPack.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Character.hpp"

DBO_INSTANTIATE_TEMPLATES(Lancelot::ParameterPack)
