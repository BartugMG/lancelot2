#pragma once
#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include "Parameter.hpp"

namespace Lancelot
{
class StatusChange;

class ParameterModel
{
public:
    ParameterModel() = default;

    explicit ParameterModel(ParameterValue parameterValue);
    ParameterModel(Parameter type, int value);

    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, typeToValue.first, "type"s);
        Wt::Dbo::field(a, typeToValue.second, "value"s);

        Wt::Dbo::belongsTo(a, m_change, "status_change"s, Wt::Dbo::OnDeleteCascade);
    }

    Parameter type() const
    {
        return typeToValue.first;
    }

    int value() const
    {
        return typeToValue.second;
    }

    ParameterValue typeToValue;

private:
    Wt::Dbo::ptr<StatusChange> m_change;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::ParameterModel)
