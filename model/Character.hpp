#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <Wt/Dbo/ptr.h>
#include "ParameterPack.hpp"
#include "CommonEnums.hpp"

namespace Lancelot
{
class User;
class ParameterPack;
class Equipment;
class StatusChange;
class Race;
class TechClass;
class MageClass;

class Character
{
public:
    Character();

    Character(std::string name, Deity deity, LifeParameters baseParams);

    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_level, "level"s);
        Wt::Dbo::field(a, m_exp, "experience"s);
        Wt::Dbo::field(a, m_deity, "deity"s);
        Wt::Dbo::field(a, m_karma, "karma"s);
        Wt::Dbo::field(a, m_alive, "is_alive"s);
        Wt::Dbo::field(a, m_baseParams.health, "base_health"s);
        Wt::Dbo::field(a, m_baseParams.mana, "base_mana"s);
        Wt::Dbo::field(a, m_baseParams.stamina, "base_stamina"s);
        Wt::Dbo::belongsTo(a, user, "user"s);
        Wt::Dbo::belongsTo(a, race, "race"s);
        Wt::Dbo::belongsTo(a, techClass, "tech_class"s);
        Wt::Dbo::belongsTo(a, mageClass, "mage_class"s);
        Wt::Dbo::hasOne(a, parameterPack);
        Wt::Dbo::hasOne(a, equipment);
        Wt::Dbo::hasMany(a, changes, Wt::Dbo::ManyToMany, "character_changes"s);
    }

    const std::string& name() const
    { return m_name; }

    Deity deity() const
    { return m_deity; }

    int level() const
    { return m_level; }

    int exp() const
    { return m_exp; }

    int karma() const
    { return m_karma; }

    int& karma()
    { return m_karma; }

    int& level()
    { return m_level; }

    int& exp()
    { return m_exp; }

    void addExp(int expAmount);

    Parameters parametersWithChangesAndEq() const;
    bool applyPotion(const Wt::Dbo::ptr<StatusChange>& change);
    void evaluateNextTurn(const LifeParameters& parametersLoss);

    Wt::Dbo::ptr<User> user;
    Wt::Dbo::ptr<Race> race;
    Wt::Dbo::ptr<TechClass> techClass;
    Wt::Dbo::ptr<MageClass> mageClass;
    Wt::Dbo::weak_ptr<ParameterPack> parameterPack;
    Wt::Dbo::weak_ptr<Equipment> equipment;
    Wt::Dbo::collection<Wt::Dbo::ptr<StatusChange>> changes;
    static constexpr int EXP_TO_LEVEL = 100;

    static inline bool potionUsedThisTurn = false; // another ugly hack

private:
    std::string m_name;
    Deity m_deity;
    int m_level = 1;
    int m_exp = 0;
    int m_karma = 100;
    bool m_alive = true;
    LifeParameters m_baseParams;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::Character)
