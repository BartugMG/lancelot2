#include "StatusChange.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Character.hpp"
#include "ParameterModel.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "Equipment.hpp"
#include "User.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "Item.hpp"
#include "Weapon.hpp"

namespace Lancelot
{
using namespace std::string_literals;

StatusChange::StatusChange(std::string name, std::string description,
                           boost::optional<int> duration):
        m_name(std::move(name)),
        m_description(std::move(description)),
        m_duration(duration)
{}

StatusChange::StatusChange():
        StatusChange(""s, ""s, boost::none)
{}


}

DBO_INSTANTIATE_TEMPLATES(Lancelot::StatusChange)
