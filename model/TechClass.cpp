#include <utility>

#include "TechClass.hpp"
#include <Wt/Dbo/Impl.h>
#include "Race.hpp"
#include "Character.hpp"
#include "Equipment.hpp"
#include "StatusChange.hpp"
#include "MageClass.hpp"
#include "User.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Potion.hpp"
#include "ParameterModel.hpp"

namespace Lancelot
{
using namespace std::string_literals;

TechClass::TechClass(TechClassEnum techClassId, ArmorClass armorClass, Parameter baseParam):
    m_id(techClassId),
    m_weaponTypes(0),
    m_armorClass(armorClass),
    m_baseParam(baseParam)
{}

TechClass::TechClass():
    TechClass(TechClassEnum::Berserker, ArmorClass::None, Parameter::Health)
{}

void TechClass::addWeaponType(WeaponType weaponType)
{
    auto bitShift = static_cast<unsigned>(weaponType);
    m_weaponTypes |= (1 << bitShift);
}

auto TechClass::getWeaponTypes() const
{
    std::vector<WeaponType> weaponTypes;

    int flags = m_weaponTypes;
    int counter = 0;
    while (flags != 0)
    {
        if (flags & 0b1)
        {
            weaponTypes.emplace_back(static_cast<WeaponType>(counter));
        }
        ++counter;
        flags >>= 1;
    }
    return weaponTypes;
}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::TechClass)
