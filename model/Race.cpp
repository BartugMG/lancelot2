#include "Race.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "StatusChange.hpp"
#include "Character.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "ParameterModel.hpp"
#include "Equipment.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "User.hpp"
#include "Item.hpp"
#include "Weapon.hpp"

namespace Lancelot
{
using namespace std::string_literals;

Race::Race(RaceEnum raceId):
    m_raceId(raceId)
{}

Race::Race():
    Race(RaceEnum::Human)
{}


}

DBO_INSTANTIATE_TEMPLATES(Lancelot::Race)
