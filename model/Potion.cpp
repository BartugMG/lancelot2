#include "Potion.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "StatusChange.hpp"
#include "Character.hpp"
#include "ParameterModel.hpp"
#include "Equipment.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "User.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "Item.hpp"
#include "Weapon.hpp"

namespace Lancelot
{
using namespace std::string_literals;

Potion::Potion():
        Potion(""s, ""s, 0, 0)
{}

Potion::Potion(std::string name, std::string description,
               int amount, double singularWeight):
    ItemBase(std::move(name), std::move(description),
             amount, singularWeight)
{}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::Potion)

