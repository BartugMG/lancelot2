#include "Item.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Equipment.hpp"

namespace Lancelot
{
using namespace std::string_literals;

Item::Item():
    Item(""s, ""s, 0, 0)
{}

Item::Item(std::string name, std::string description,
           int amount, double singularWeight):
    ItemBase(std::move(name), std::move(description),
             amount, singularWeight)
{}

}


DBO_INSTANTIATE_TEMPLATES(Lancelot::Item)

