#include "Jewellery.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Equipment.hpp"
#include "StatusChange.hpp"
#include "Character.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "ParameterModel.hpp"
#include "Race.hpp"
#include "User.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "Item.hpp"
#include "Weapon.hpp"

namespace Lancelot
{
using namespace std::string_literals;

Jewellery::Jewellery(std::string name, std::string description, int amount,
                     double singularWeight, JewelleryType type):
    ItemBase(std::move(name), std::move(description),
             amount, singularWeight),
    m_type(type),
    m_isEquipped(false)
{}

Jewellery::Jewellery():
        Jewellery(""s, ""s, 0, 0, JewelleryType::Amulet)
{}

void Jewellery::flipEquippedState()
{
    m_isEquipped = !m_isEquipped;
}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::Jewellery)
