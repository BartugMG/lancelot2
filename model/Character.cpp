#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Character.hpp"
#include "User.hpp"
#include "ParameterPack.hpp"
#include "Equipment.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"
#include "Pouch.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include <Wt/Dbo/Transaction.h>
#include "LancelotApp.hpp"
#include <cmath>


namespace Lancelot
{
using namespace std::string_literals;

namespace
{
constexpr auto BASE_PARAM_RATIO = 0.6;
constexpr auto SECONDARY_PARAM_RATIO = 1. - BASE_PARAM_RATIO ;

int calculateAbility(Parameter baseParam, int strOrInt, int agiOrChar)
{
    auto baseStat = strOrInt;
    auto secondaryStat = agiOrChar;

    if (baseParam == Parameter::Charisma or baseParam == Parameter::Agility)
    {
        std::swap(baseStat, secondaryStat);
    }

    return static_cast<int>(ceil(BASE_PARAM_RATIO * baseStat + SECONDARY_PARAM_RATIO * secondaryStat));
}

bool isLifeParam(Parameter param)
{
   return param == Parameter::Health or
          param == Parameter::Mana or
          param == Parameter::Stamina;
}

}

Character::Character():
        Character(""s, Deity::Laselis, LifeParameters{})
{}

Character::Character(std::string name, Deity deity, LifeParameters baseParams):
    m_name(std::move(name)),
    m_deity(deity),
    m_baseParams(baseParams)
{}

void Character::addExp(int expAmount)
{
    m_exp += expAmount;
    if (m_exp >= EXP_TO_LEVEL)
    {
        ++m_level;
        m_exp -= EXP_TO_LEVEL;
    }
}

Parameters Character::parametersWithChangesAndEq() const
{
    // this should be probably cached somewhere and update only according to change but well...
    auto pack = parameterPack->params();

    for (const auto& change : race->changes)
    {
        for (const auto& param : change->params)
        {
            pack.applyModifier(param->typeToValue);
        }
    }

    for (const auto& change : changes)
    {
        for (const auto& paramChange : change->params)
        {
            // this if means that status change is temporary and should not modify life param
            // at the end of the turn it will be added or subtracted from stats in different class
            if (change->duration().is_initialized() and not isLifeParam(paramChange->type()))
            {
                pack.applyModifier(paramChange->typeToValue);
            }
        }
    }

    auto& armor = equipment.modify()->armor;
    for (const auto& armorPiece : armor)
    {
        if (armorPiece->isEquipped())
        {
            for (const auto& paramChange : armorPiece->statusChange.modify()->params)
            {
                pack.applyModifier(paramChange->typeToValue);
            }
        }
    }

    auto& jewellery = equipment.modify()->jewellery;
    for (const auto& jewel : jewellery)
    {
        if (jewel->isEquipped())
        {
            for (const auto& paramChange : jewel->statusChange.modify()->params)
            {
                pack.applyModifier(paramChange->typeToValue);
            }
        }
    }

    pack.techAbility() = calculateAbility(techClass->baseParam(), pack.strength(), pack.agility());
    pack.magicAbility() = calculateAbility(mageClass->baseParam(), pack.intelligence(), pack.charisma());

    if(race->asEnum() == RaceEnum::Drow)
    {
        ++pack.magicAbility();
    }

    return pack;
}


void Character::evaluateNextTurn(const LifeParameters& parametersLoss)
{
    auto pack = parameterPack->params();
    for (const auto& change : changes)
    {
        if (change->duration().is_initialized())
        {
            for (const auto& param : change->params)
            {
                if (isLifeParam(param->type()))
                {
                    pack.applyModifier(param->typeToValue, m_baseParams);
                }
            }

            change.modify()->decrementDuration();
        }
    }

    for(auto& change : changes)
    {
        if (change->duration().is_initialized() and change->duration() == 0)
        {
            change.remove();
        }
    }

    pack.applyModifier(ParameterValue{Parameter::Health, -parametersLoss.health}, m_baseParams);
    pack.applyModifier(ParameterValue{Parameter::Mana, -parametersLoss.mana}, m_baseParams);
    pack.applyModifier(ParameterValue{Parameter::Stamina, -parametersLoss.stamina}, m_baseParams);

    parameterPack.modify()->setParams(pack);
    potionUsedThisTurn = false;
}

bool Character::applyPotion(const Wt::Dbo::ptr<StatusChange>& change)
{
    if (potionUsedThisTurn)
    {
        return false;
    }

    // ugh...
    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);
    auto copyChange = session.add(std::make_unique<StatusChange>(
            change->name(), change->description(), change->duration()));
    for(const auto& param : change->params)
    {
        auto copyParam = session.add(std::make_unique<ParameterModel>(param->typeToValue));
        copyChange.modify()->params.insert(copyParam);
    }

    changes.insert(copyChange);
    potionUsedThisTurn = true;

    return true;
}


}


DBO_INSTANTIATE_TEMPLATES(Lancelot::Character)
