#pragma once

#include "ItemBase.hpp"
#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <boost/optional.hpp>
#include "Dice.hpp"
#include "CommonEnums.hpp"

namespace Lancelot
{
class Equipment;

class Weapon : public ItemBase
{
public:
    Weapon();

    Weapon(std::string name, std::string description, int amount,
           double singularWeight, WeaponType type, Dice dice,
           int modifier, boost::optional<int> durability);

    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_description, "description"s);
        Wt::Dbo::field(a, m_amount, "amount"s);
        Wt::Dbo::field(a, m_singularWeight, "weight"s);

        Wt::Dbo::field(a, m_type, "wtype"s);
        Wt::Dbo::field(a, m_dice.dices, "dices"s);
        Wt::Dbo::field(a, m_dice.sides, "sides"s);
        Wt::Dbo::field(a, m_modifier, "modifier"s);
        Wt::Dbo::field(a, m_isEquipped, "equipped"s);
        Wt::Dbo::field(a, m_durability, "durability"s);

        Wt::Dbo::belongsTo(a, m_eq, "equipment"s);
    }

    WeaponType type() const
    { return m_type; }

    const Dice& dice() const
    { return m_dice; }

    int modifier() const
    { return m_modifier; }

    bool isEquipped() const
    { return m_isEquipped; }

    const boost::optional<int>& durability() const
    { return m_durability; }

    void flipEquippedState();

private:
    WeaponType m_type;
    Dice m_dice;
    int m_modifier;
    boost::optional<int> m_durability;
    bool m_isEquipped;

    Wt::Dbo::ptr<Equipment> m_eq;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::Weapon)
