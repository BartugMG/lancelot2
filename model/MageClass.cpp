#include <utility>

#include "MageClass.hpp"
#include <Wt/Dbo/Impl.h>
#include "Race.hpp"
#include "Character.hpp"
#include "Equipment.hpp"
#include "StatusChange.hpp"
#include "TechClass.hpp"
#include "User.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Potion.hpp"
#include "ParameterModel.hpp"

namespace Lancelot
{
using namespace std::string_literals;

MageClass::MageClass(MageClassEnum mageClassId, Parameter baseParam):
    m_id(mageClassId),
    m_baseParam(baseParam),
    m_deities(0)
{}

MageClass::MageClass():
    MageClass(MageClassEnum::Alchemist, Parameter::Health)
{}

void MageClass::addDeity(Deity deity)
{
    auto bitShift = static_cast<unsigned>(deity);
    m_deities |= (1 << bitShift);
}

auto MageClass::getDeities() const
{
    std::vector<Deity> deities;

    int flags = m_deities;
    int counter = 0;
    while (flags != 0)
    {
        if (flags & 0b1)
        {
            deities.emplace_back(static_cast<Deity>(counter));
        }
        ++counter;
        flags >>= 1;
    }
    return deities;
}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::MageClass)
