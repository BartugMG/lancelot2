#pragma once
#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include "Parameter.hpp"

namespace Lancelot
{
class Character;

class ParameterPack
{
public:
    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_params.health(), "health"s);
        Wt::Dbo::field(a, m_params.mana(), "mana"s);
        Wt::Dbo::field(a, m_params.stamina(), "stamina"s);
        Wt::Dbo::field(a, m_params.strength(), "strength"s);
        Wt::Dbo::field(a, m_params.agility(), "agility"s);
        Wt::Dbo::field(a, m_params.intelligence(), "intelligence"s);
        Wt::Dbo::field(a, m_params.charisma(), "charisma"s);
        Wt::Dbo::field(a, m_params.techAbility(), "techAbility"s);
        Wt::Dbo::field(a, m_params.magicAbility(), "magicAbility"s);
        Wt::Dbo::belongsTo(a, m_character, "character"s);
    }

    const Parameters& params() const
    { return m_params; }

    void setParams(const Parameters& params)
    { m_params = params; }

private:
    Parameters m_params;
    Wt::Dbo::ptr<Character> m_character;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::ParameterPack)
