#include "ParameterModel.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "StatusChange.hpp"
#include "Character.hpp"

namespace Lancelot
{

ParameterModel::ParameterModel(Parameter type, int value):
    typeToValue(std::pair(type, value))
{}

ParameterModel::ParameterModel(ParameterValue parameterValue):
    typeToValue(std::move(parameterValue))
{}

}

DBO_INSTANTIATE_TEMPLATES(Lancelot::ParameterModel)

