#pragma once

#include <cassert>
#include <string>

namespace Lancelot
{

struct Dice
{
    unsigned roll() const
    {
        // TODO
        assert(dices >= 0);
        assert(sides >= 0);
        return static_cast<unsigned>(dices);
    }

    std::string asString() const
    {
        using namespace std::string_literals;

        return std::to_string(dices) + "k"s + std::to_string(sides);
    }

    int dices;
    int sides;
};

}

