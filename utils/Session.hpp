#pragma once

#include <Wt/Auth/Login.h>
#include <Wt/Auth/Dbo/UserDatabase.h>
#include <Wt/Dbo/Session.h>
#include <Wt/Dbo/ptr.h>
#include "User.hpp"

namespace Lancelot
{

using UserDatabase = Wt::Auth::Dbo::UserDatabase<AuthInfo>;
namespace Dbo = Wt::Dbo;

class Session : public Dbo::Session
{
public:
    explicit Session(const std::string& sqliteDb);

    Wt::Auth::AbstractUserDatabase& users()
    { return *m_users; }

    Wt::Auth::Login& login()
    { return m_login; }

    Dbo::ptr<User> user();
    
    static void configureAuth();
    static Wt::Auth::AuthService& authService();
    static Wt::Auth::PasswordService& passwordService();

private:
    std::unique_ptr<UserDatabase> m_users;
    Wt::Auth::Login m_login;

    Dbo::ptr<User> findExistingElseCreateNewUser();
};

}
