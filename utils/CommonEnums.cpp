#include "CommonEnums.hpp"

namespace Lancelot
{
using namespace std;

string toString(ArmorType at)
{
    switch (at)
    {
        case ArmorType::Full:    return "Zbroja pełna"s;
        case ArmorType::Helmet:  return "Hełm"s;
        case ArmorType::Cuirass: return "Napierśnik"s;
        case ArmorType::Gloves:  return "Rękawice"s;
        case ArmorType::Legs:    return "Nogawice"s;
        case ArmorType::Boots:   return "Buty"s;
        case ArmorType::Shield:  return "Tarcza"s;
    }

    return "???"s;
}

string toString(ArmorClass ac)
{
    switch(ac)
    {
        case ArmorClass::None:   return "Brak"s;
        case ArmorClass::Light:  return "Lekki"s;
        case ArmorClass::Medium: return "Średni"s;
        case ArmorClass::Heavy:  return "Ciężki"s;
    }

    return "???"s;
}

std::string toString(CoinType c)
{
    switch (c)
    {
        case CoinType::Platinum: return "Platyna"s;
        case CoinType::Gold:     return "Złoto"s;
        case CoinType::Silver:   return "Srebro"s;
        case CoinType::Copper:   return "Miedź"s;
    }

    return "???";
}

string toString(Deity d)
{
    switch (d)
    {
        case Deity::Laselis:   return "Laselis"s;
        case Deity::Dethelion: return "Dethelion"s;
        case Deity::Aquest:    return "Aquest"s;
        case Deity::Nateis:    return "Nateis"s;
        case Deity::Ferlar:    return "Ferlar"s;
        case Deity::Arles:     return "Arles"s;
    }

    return "???"s;
}

string toString(JewelleryType j)
{
    switch (j)
    {
        case JewelleryType::Amulet: return "Amulet"s;
        case JewelleryType::Ring:   return "Pierścień"s;
    }

    return "???"s;
}

std::string toString(Parameter p)
{
    switch (p)
    {
        case Parameter::Health:       return "PZ";
        case Parameter::Mana:         return "PM";
        case Parameter::Stamina:      return "PK";
        case Parameter::Strength:     return "Siła";
        case Parameter::Agility:      return "Zręczność";
        case Parameter::Intelligence: return "Inteligencja";
        case Parameter::Charisma:     return "Charyzma";
        case Parameter::TechAbility:  return "ZT";
        case Parameter::MagicAbility: return "ZM";
    }

    return "???";
}

std::string toString(WeaponType w)
{
    switch(w)
    {
        case WeaponType::Dagger:     return "Sztylet"s;
        case WeaponType::ShortSword: return "Krótki miecz"s;
        case WeaponType::Sword:      return "Miecz"s;
        case WeaponType::Axe:        return "Topór"s;
        case WeaponType::Hammer:     return "Młot"s;
        case WeaponType::TwoHSword:  return "Miecz dwuręczny"s;
        case WeaponType::TwoHAxe:    return "Topór dwuręczny"s;
        case WeaponType::TwoHHammer: return "Młot dwuręczny"s;
        case WeaponType::Lance:      return "Włócznia"s;
        case WeaponType::Kusarigama: return "Kusarigama"s;
        case WeaponType::Bow:        return "Łuk"s;
        case WeaponType::Crossbow:   return "Kusza"s;
        case WeaponType::Arrow:      return "Strzała"s;
        case WeaponType::Bolt:       return "Bełt"s;
        case WeaponType::Gun:        return "Broń palna"s;
        case WeaponType::Grenade:    return "Granat"s;
        case WeaponType::Other:      return "Inne"s;
        case WeaponType::Ultimate:   return "Broń specjalna"s;
        case WeaponType::Thrown:     return "Broń miotana"s;
    };

    return "???"s;
}

std::string toString(RaceEnum r)
{
    switch (r)
    {
        case RaceEnum::Human:   return "Człowiek"s;
        case RaceEnum::Elf:     return "Elf"s;
        case RaceEnum::Itichan: return "Itichan"s;
        case RaceEnum::Dwarf:   return "Krasnolud"s;
        case RaceEnum::Drow:    return "Drow"s;
        case RaceEnum::Rhoden:  return "Rhoden"s;
    }

    return "???"s;
}

std::string toString(TechClassEnum t)
{
    switch (t)
    {
        case TechClassEnum::Berserker:   return "Berserker"s;
        case TechClassEnum::HeavyArmed:  return "Ciężkozbrojny"s;
        case TechClassEnum::BladeDancer: return "Tancerz ostrzy"s;
        case TechClassEnum::Agent:       return "Agent"s;
        case TechClassEnum::Scout:       return "Zwiadowca"s;
        case TechClassEnum::Lancer:      return "Lansjer"s;
    }

    return "???"s;
}

std::string toString(MageClassEnum m)
{
    switch (m)
    {
        case MageClassEnum::Alchemist: return "Alchemik"s;
        case MageClassEnum::Engineer:  return "Inżynier"s;
        case MageClassEnum::Enhancer:  return "Zaklinacz"s;
        case MageClassEnum::Mistique:  return "Mistyk"s;
        case MageClassEnum::Summoner:  return "Przywoływacz"s;
        case MageClassEnum::Shaman:    return "Szaman"s;
    }

    return "???"s;
}

}
