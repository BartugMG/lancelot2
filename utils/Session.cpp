#include <Wt/Auth/AuthService.h>
#include <Wt/Auth/HashFunction.h>
#include <Wt/Auth/PasswordService.h>
#include <Wt/Auth/PasswordStrengthValidator.h>
#include <Wt/Auth/PasswordVerifier.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include "Session.hpp"
#include "Character.hpp"
#include "ParameterPack.hpp"
#include "Equipment.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "ParameterModel.hpp"
#include "StatusChange.hpp"
#include "Pouch.hpp"
#include "Potion.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"
#include "Race.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"


namespace Lancelot
{
using namespace Wt;
using namespace std::string_literals;

Session::Session(const std::string& sqliteDb)
{
    auto connection = std::make_unique<Dbo::backend::Sqlite3>(sqliteDb);
    connection->setProperty("show-queries"s, "true");
    setConnection(std::move(connection));

    mapClass<User>("user");
    mapClass<AuthInfo>("auth_info");
    mapClass<AuthInfo::AuthIdentityType>("auth_identity");
    mapClass<AuthInfo::AuthTokenType>("auth_token");
    mapClass<Character>("character");
    mapClass<ParameterPack>("param_pack");
    mapClass<Equipment>("equipment");
    mapClass<Item>("item");
    mapClass<Weapon>("weapon");
    mapClass<ParameterModel>("param");
    mapClass<StatusChange>("status_change");
    mapClass<Pouch>("pouch");
    mapClass<Potion>("potion");
    mapClass<Armor>("armor");
    mapClass<Jewellery>("jewellery");
    mapClass<Race>("race");
    mapClass<TechClass>("tech_class");
    mapClass<MageClass>("mage_class");

    try
    {
        createTables();
        std::cerr << "Created database" << std::endl;
    }
    catch(std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << "Using existing database" << std::endl;
    }

    m_users = std::make_unique<UserDatabase>(*this);
}

Dbo::ptr<User> Session::user()
{
    if(m_login.loggedIn())
    {
        return findExistingElseCreateNewUser();
    }
    else
    {
        return Dbo::ptr<User>();
    }
}

void Session::configureAuth()
{
    authService().setAuthTokensEnabled(true, "logincookie"s);
    authService().setEmailVerificationEnabled(false);
    // TODO uncomment after test phase
    //authService().setEmailVerificationRequired(true);

    auto verifier = std::make_unique<Auth::PasswordVerifier>();
    verifier->addHashFunction(std::make_unique<Auth::BCryptHashFunction>(7));
    passwordService().setVerifier(std::move(verifier));
    passwordService().setAttemptThrottlingEnabled(true);
    //passwordService().setStrengthValidator(std::make_unique<Auth::PasswordStrengthValidator>());
}

Wt::Auth::AuthService& Session::authService()
{
    static Auth::AuthService authService;
    return authService;
}

Wt::Auth::PasswordService& Session::passwordService()
{
    static Auth::PasswordService passwordService(authService());
    return passwordService;
}

Dbo::ptr<User> Session::findExistingElseCreateNewUser()
{
    auto authInfo = m_users->find(m_login.user());

    auto user = authInfo->user();
    if(!user)
    {
        user = add(std::make_unique<User>());
        user.modify()->setRole(Role::USER);
        authInfo.modify()->setUser(user);
    }
    return user;
}


}
