#pragma once
#include <tuple>
#include <map>
#include <iostream>
#include "Utility.hpp"
#include "CommonEnums.hpp"

namespace Lancelot
{
using ParameterValue = std::pair<Parameter, int>;

/// I would use this struct in Parameters class, but I won't develop
/// this app any more so it would be waste of time
struct LifeParameters
{
    int health = 0;
    int mana = 0;
    int stamina = 0;
};


class Parameters
{
public:
    Parameters();

    int& health()
    { return m_paramValues.at(Parameter::Health); }

    int& mana()
    { return m_paramValues.at(Parameter::Mana); }
    int& stamina()
    { return m_paramValues.at(Parameter::Stamina); }

    int& strength()
    { return m_paramValues.at(Parameter::Strength); }

    int& agility()
    { return m_paramValues.at(Parameter::Agility); }

    int& intelligence()
    { return m_paramValues.at(Parameter::Intelligence); }

    int& charisma()
    { return m_paramValues.at(Parameter::Charisma); }


    int& techAbility()
    { return m_paramValues.at(Parameter::TechAbility); }

    int& magicAbility()
    { return m_paramValues.at(Parameter::MagicAbility); }


    void applyModifier(const ParameterValue& modifierToApply);
    void applyModifier(const ParameterValue& modifierToApply, const LifeParameters& maxParams);

private:
    std::map<Parameter, int> m_paramValues;

};

}
