#pragma once

#include <array>
#include <Wt/Dbo/ptr.h>
#include "CommonEnums.hpp"
#include "Race.hpp"
#include "TechClass.hpp"
#include "MageClass.hpp"
#include "LancelotApp.hpp"

namespace Lancelot
{
using RaceArray = std::array<Wt::Dbo::ptr<Race>, 6>;
using TechClassArray = std::array<Wt::Dbo::ptr<TechClass>, 6>;
using MageClassArray = std::array<Wt::Dbo::ptr<MageClass>, 6>;

class TechClassFillers
{
public:
    TechClassFillers();

    TechClassArray techClasses;

private:
    Session& m_session;
    void addBerserker();
    void addHeavyArmed();
    void addBladeDancer();
    void addAgent();
    void addLancer();
    void addScout();
    void getExistingTechClassesFromDB();
};

class MageClassFillers
{
public:
    MageClassFillers();

    MageClassArray mageClasses;

private:
    Session& m_session;
    void addAlchemist();
    void addEngineer();
    void addEnhancer();
    void addMistique();
    void addSummoner();
    void addShaman();
    void getExistingMageClassesFromDB();
    void fillEveryDeity(Wt::Dbo::ptr<MageClass>& mageClass);
};

class RaceFillers
{
public:
    RaceFillers(const TechClassArray& techClasses, const MageClassArray& mageClasses);

    RaceArray races;

private:
    Session& m_session;
    const TechClassArray& m_techClasses;
    const MageClassArray& m_mageClasses;
    void addHuman();
    void addRhoden();
    void addItichan();
    void addElf();
    void addDwarf();
    void addDrow();
    void getExistingRacesFromDB();
};

class AgregateModelsFiller
{
private:
    TechClassFillers m_techClassFillers;
    MageClassFillers m_mageClassFillers;
    RaceFillers m_raceFillers;

public:
    AgregateModelsFiller();

    static AgregateModelsFiller& instance();

    TechClassArray& techClasses;
    MageClassArray& mageClasses;
    RaceArray& races;
};


}
