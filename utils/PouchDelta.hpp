#pragma once

struct PouchDelta
{
    int plat;
    int gold;
    int silver;
    int copper;
};