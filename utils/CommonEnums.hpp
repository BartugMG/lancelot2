#pragma once

#include <string>
#include <array>

namespace Lancelot
{

enum class ArmorType
{
    Full,
    Helmet,
    Cuirass,
    Gloves,
    Legs,
    Boots,
    Shield
};


enum class ArmorClass
{
    None,
    Light,
    Medium,
    Heavy
};

enum class CoinType
{
    Platinum,
    Gold,
    Silver,
    Copper
};

enum class Deity
{
    Laselis,
    Dethelion,
    Aquest,
    Nateis,
    Ferlar,
    Arles
};

constexpr std::array<Deity, 6> DEITIES
{
   Deity::Laselis, Deity::Dethelion, Deity::Aquest,
   Deity::Nateis, Deity::Ferlar, Deity::Arles
};

enum class JewelleryType
{
    Amulet,
    Ring
};

enum class Parameter
{
    Health,
    Mana,
    Stamina,

    Strength,
    Agility,
    Intelligence,
    Charisma,

    TechAbility,
    MagicAbility
};

enum class WeaponType
{
    Dagger,
    ShortSword,
    Sword,
    Axe,
    Hammer,
    TwoHSword,
    TwoHAxe,
    TwoHHammer,
    Thrown,
    Lance,
    Kusarigama,
    Bow,
    Crossbow,
    Arrow,
    Bolt,
    Gun,
    Grenade,
    Other,
    Ultimate
};

enum RaceEnum
{
    Human,
    Elf,
    Itichan,
    Dwarf,
    Drow,
    Rhoden
};

constexpr std::array<RaceEnum, 6> RACES
{
    RaceEnum::Human, RaceEnum::Elf, RaceEnum::Itichan,
    RaceEnum::Dwarf, RaceEnum::Drow, RaceEnum::Rhoden
};

enum TechClassEnum
{
    Berserker,
    HeavyArmed,
    BladeDancer,
    Agent,
    Scout,
    Lancer
};

constexpr std::array<TechClassEnum, 6> TECH_CLASSES
{
    TechClassEnum::Berserker, TechClassEnum::HeavyArmed,
    TechClassEnum::BladeDancer, TechClassEnum::Agent,
    TechClassEnum::Scout, TechClassEnum::Lancer
};

enum MageClassEnum
{
    Alchemist,
    Engineer,
    Enhancer,
    Mistique,
    Summoner,
    Shaman
};

constexpr  std::array<MageClassEnum, 6> MAGE_CLASSES
{
    MageClassEnum::Alchemist, MageClassEnum::Engineer,
    MageClassEnum::Enhancer, MageClassEnum::Mistique,
    MageClassEnum::Summoner, MageClassEnum::Shaman
};

std::string toString(ArmorType at);
std::string toString(ArmorClass ac);
std::string toString(CoinType c);
std::string toString(Deity d);
std::string toString(JewelleryType j);
std::string toString(Parameter p);
std::string toString(WeaponType w);
std::string toString(RaceEnum r);
std::string toString(TechClassEnum t);
std::string toString(MageClassEnum m);

}
