#include "Parameter.hpp"
#include <array>

namespace Lancelot
{
void Parameters::applyModifier(const ParameterValue& modifierToApply)
{
    const auto& [param, modifier] = modifierToApply;

    auto& paramValue = m_paramValues.at(param);
    paramValue = std::max(0, paramValue+modifier);
}

void Parameters::applyModifier(const ParameterValue& modifierToApply, const LifeParameters& maxParams)
{
    const auto& [param, modifier] = modifierToApply;

    switch (auto& paramValue = m_paramValues.at(param); param)
    {
        case Parameter::Health:
            paramValue = std::clamp(paramValue+modifier, 0, maxParams.health);
            break;
        case Parameter::Mana:
            paramValue = std::clamp(paramValue+modifier, 0, maxParams.mana);
            break;
        case Parameter::Stamina:
            paramValue = std::clamp(paramValue+modifier, 0, maxParams.stamina);
            break;
        default:
            applyModifier(modifierToApply);
            break;
    }
}

Parameters::Parameters()
{
    Utility::valueEmplacer(m_paramValues, 0,
                           Parameter::Health, Parameter::Mana, Parameter::Stamina,
                           Parameter::Strength, Parameter::Agility, Parameter::Intelligence,
                           Parameter::Charisma, Parameter::TechAbility, Parameter::MagicAbility);
}

}
