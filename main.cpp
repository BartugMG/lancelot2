#include "LancelotApp.hpp"
#include "Session.hpp"

int main(int argc, char** argv)
{
    try
    {
        Wt::WServer server{argc, argv, WTHTTP_CONFIGURATION};

        server.addEntryPoint(Wt::EntryPointType::Application, Lancelot::LancelotApp::createApp);

        Lancelot::Session::configureAuth();

        server.run();
    }
    catch (Wt::WServer::Exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    catch (Wt::Dbo::Exception &e)
    {
        std::cerr << "Dbo exception: " << e.what() << std::endl;
    }
    catch (std::exception &e)
    {
        std::cerr << "exception: " << e.what() << std::endl;
    }

    return 0;
}
